Downloading Firmware
####################

The LVFS has supplied over 100,000,000 files to end users, mostly semi-automatically downloaded by
software centers and various other firmware update frameworks.
To ensure the LVFS service works correctly, and is cost effective to run, we do have some rules
that clients should follow:

User Agent
==========

Firmware update programs downloading archives from the LVFS should provide a valid user agent.
This should include:

 * client name, e.g. ``gnome-software/46.2``
 * system description, e.g. ``Linux x86_64``
 * user language, e.g. ``en-US``
 * operating system, e.g. ``Fedora Linux 40``
 * the fwupd daemon **runtime** version, e.g. ``fwupd/1.9.21``

This allows us to prevent updates going to systems where the updates will not work,
or where they may damage the system.

For instance, dbx updates may not be deployed currently on Silverblue atomic systems,
and some old fwupd versions have serious bugs that are detected server side.
Bugs such as this can be worked around by serving different firmware payloads.

Although the language field is not currently used, in the future the LVFS may start providing
localized update description text, falling back to English where this is not available.

The user agent is mainly used to identify clients that are abusing the LVFS web service but it also
allows the LVFS team to decide when to end-of-life fwupd major versions.

Good user agent examples:

 * ``fwupdmgr/1.7.9 (Linux x86_64 5.15.0-113-generic; en-US; Ubuntu 22.04) fwupd/1.7.9``
 * ``gnome-software/46.2 (Linux x86_64 6.9.5-200.fc40.x86_64; en-US; Fedora Linux 40 Silverblue) fwupd/1.9.21``
 * ``plasma-discover/5.27.11 (Linux x86_64 6.9.7-100.fc39.x86_64; de-DE; Fedora Linux 39 KDE Plasma) fwupd/2.0.0``

Bad user agent examples:

 * ``firmware-updater``
 * ``fwupd/1.9.5``

Redirects
=========

The LVFS auto-mirrors popular firmware files to save on bandwidth egress cost, and clients should
expect to do a client-side redirect to obtain the firmware archive.

Download Limits
===============

The LVFS automatically blocks IPs downloading the same file more than 250 times in a 24 hour period.
IPs may also be manually blocked if they are making suspicious or damaging queries.
Examples include causing too much server load or downloading thousands of files in parallel.
