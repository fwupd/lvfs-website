#!/usr/bin/python3
#
# Copyright 2024 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods,unused-argument,protected-access

from typing import Optional
from sqlalchemy.exc import IntegrityError

from lvfs import db
from lvfs.tasks.models import Task

from .models import Client


def _fsck_clients_delete(task: Task) -> None:

    try:
        db.session.query(Client).filter(Client.user_agent.like("%azure%")).delete(
            synchronize_session=False
        )
        db.session.commit()
    except IntegrityError as e:
        db.session.rollback()
        task.add_fail(
            "Database::Clients",
            f"Cannot delete CI clients: {e!s}",
        )


def _fsck_all(task: Task, kinds: Optional[list[str]] = None) -> None:
    if not kinds or "delete" in kinds:
        _fsck_clients_delete(task)
