#!/usr/bin/python3
#
# Copyright 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=unused-argument

import datetime
from typing import Optional, Tuple, Any

from flask import render_template, url_for
from flask import current_app as app

from pkgversion import vercmp

from lvfs import db

from lvfs.emails import send_email_sync
from lvfs.components.models import ComponentChecksum
from lvfs.issues.models import Issue
from lvfs.tests.models import Test
from lvfs.firmware.models import (
    FirmwareEvent,
    FirmwareEventKind,
    Firmware,
    FirmwareReleaseFlag,
    FirmwareRevision,
)
from lvfs.metadata.models import Remote
from lvfs.users.models import User
from lvfs.tasks.models import Task
from lvfs.hash import _is_sha1, _is_sha256, _is_sha384
from lvfs.util import _get_client_address

from .models import Report, ReportAttribute, REPORT_ATTR_MAP


def _ensure_system_integrity_test_failure(report: Report) -> None:

    # no data
    if not report.system_integrity:
        return

    # ensure test exists
    test: Optional[Test] = report.fw.find_test_by_plugin_id("system_integrity")
    if not test:
        test = Test(
            plugin_id="system_integrity",
            waivable=True,
            started_ts=datetime.datetime.utcnow(),
            ended_ts=datetime.datetime.utcnow(),
        )
        report.fw.tests.append(test)

    # build a string
    for key in report.system_integrity:
        old, new = report.system_integrity[key]
        if old and new:
            if key == "UEFI:OsIndicationsSupported":
                test.add_fail(
                    "Update Mechanism Changed",
                    " ".join(
                        [
                            "The firmware update changed the supported capsule update mechanisms, "
                            "for instance, removing or adding support for CapsuleOnDisk.",
                            "If this was done intentionally then firmware should be carefully "
                            "tested with the new capsule application mode.",
                            "If CapsuleOnDisk support is defined then it is currently used by "
                            "default in preference to the NVRAM fwupd.efi loader.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "ACPI:TPM2":
                test.add_warn(
                    "TPM Mechanism Changed",
                    " ".join(
                        [
                            "This ACPI table is used to define the mechanism for communicating "
                            "between the TPM 2.0 device and the operating system.",
                            "It is not expected to change during a firmware update.",
                            "If changed, the full TPM operation must be retested in all supported "
                            "operating systems, also ensuring that PCRs do not change.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key in ["ACPI:MSDM", "ACPI:SLIC"]:
                test.add_fail(
                    "OEM Activation Key Changed",
                    " ".join(
                        [
                            "The firmware update modified the Windows OEM activation key stored "
                            "in NVRAM.",
                            "This may indicate that the key has been reset or cleared, "
                            "meaning that Windows will ask the user for the installation key "
                            "rather than automatically getting the OEM key from the hardware itself.",
                            "This generally makes users unhappy.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "UEFI:VendorKeys":
                test.add_fail(
                    "Vendor Keys Changed",
                    " ".join(
                        [
                            "The firmware update modified the keys enrolled by the user into the "
                            "Secure Boot db store.",
                            "This might indicate the FW update has erased the user's "
                            "manually-enrolled keys.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "UEFI:dbx":
                test.add_fail(
                    "dbx Key Changed",
                    " ".join(
                        [
                            "The firmware update updated the list of blocked SecureBoot "
                            "Authenticode hashes.",
                            "This is normally done by the user-initiated OS dbx update, "
                            "which has the ability to check the contents of each ESP before "
                            "deploying.",
                            "Including the new dbx in a firmware update means that older operating "
                            "systems (or ones that do not have their bootloader updated) may fail "
                            "to boot after the firmware update has been deployed.",
                            "Additionally, the recovery or backup media will also fail to work.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "UEFI:SetupMode":
                test.add_fail(
                    "UEFI Setup Mode changed",
                    " ".join(
                        [
                            "The firmware update changed the UEFI SetupMode variable.",
                            "Changes in the SetupMode can impact directly in the authentication of "
                            "several UEFI requests such as enrolling PKs, KEKs and OS Recovery.",
                            "This could affect Secure Boot verification.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "UEFI:PK":
                test.add_fail(
                    "Platform Key Changed",
                    " ".join(
                        [
                            "The firmware update changed the UEFI Platform Key.",
                            "Changes to the PK likely impact TPM PCR 7 measurements, which could "
                            "result in the OS being unable to access data sealed against the TPM, "
                            "with resulting unhappy users.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "UEFI:DeployedMode":
                test.add_fail(
                    "Deployed Mode Changed",
                    " ".join(
                        [
                            "The firmware update changed the DeployedMode variable.",
                            "This change impacts directly on the security level of the validations "
                            "performed by the firmware.",
                            "Unexpected changes to this mode could affect Secure Boot verification.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "UEFI:KEKDefault":
                test.add_warn(
                    "Default KEK Changed",
                    " ".join(
                        [
                            "The firmware update changed the “factory reset” KEK, "
                            "which is used when resetting the machine back to the original configuration.",
                            "If this was done intentionally then supported operating systems should "
                            "be carefully tested after resetting all the UEFI keys to their default values.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "UEFI:SecureBoot":
                test.add_warn(
                    "SecureBoot State Changed",
                    " ".join(
                        [
                            "The firmware update may have modified the enabled state of UEFI Secure Boot.",
                            "The user may have manually disabled UEFI Secure Boot before running the "
                            "capsule loader although this should not be required.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            elif key == "UEFI:BootCurrent":
                test.add_warn(
                    "Boot Target Changed",
                    " ".join(
                        [
                            "The firmware update may have rearranged the boot entries, "
                            "which it wasn’t supposed to do according to the UEFI specification.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            else:
                test.add_warn(
                    f"Changed {key}",
                    f"Modified from {old} to {new}",
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
        elif old:
            if key.startswith("UEFI:Boot00"):
                test.add_fail(
                    "Boot Entry Removed",
                    " ".join(
                        [
                            f"The firmware update removed the {key[5:]} entry, "
                            "which it wasn’t supposed to do according to the UEFI specification.",
                            "If the full ROM was wiped during the update, "
                            "these entries should be saved and restored.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            else:
                test.add_warn(
                    f"Removed {key}",
                    f"Old value was {old}",
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
        elif new:
            if key == "UEFI:OsIndicationsSupported":
                test.add_fail(
                    "Update Mechanism Changed",
                    " ".join(
                        [
                            "The firmware update added a specific supported capsule update mechanism, "
                            "for instance, adding support for CapsuleOnDisk.",
                            "If this was done intentionally then firmware should be carefully "
                            "tested with the new capsule application mode.",
                            "If CapsuleOnDisk support is defined then it is currently used by "
                            "default in preference to the NVRAM fwupd.efi loader.",
                        ]
                    ),
                    url=url_for("reports.route_show", report_id=report.report_id),
                )
            test.add_warn(
                f"Added {key}",
                f"New value  {new}",
                url=url_for("reports.route_show", report_id=report.report_id),
            )


def _find_issue_for_report_data(data: dict, fw: Firmware) -> Optional[Issue]:
    for issue in db.session.query(Issue).order_by(Issue.priority.desc()):
        if not issue.enabled:
            continue
        if issue.vendor_id not in (1, fw.vendor_id):
            continue
        if issue.matches(data):
            return issue
    return None


def reports_process_upload_data(
    item: dict[Any, Any], user: Optional[User] = None
) -> Tuple[list[str], list[str]]:
    """Upload a report"""

    msgs: list[str] = []
    uris: list[str] = []

    # check we got enough data
    for key in ["Reports", "Metadata"]:
        if key not in item:
            raise ValueError(f"invalid data, expected {key}")
        if item[key] is None:
            raise ValueError(f"missing data, expected {key}")

    # add each firmware report
    machine_id = item["MachineId"]
    reports = item["Reports"]
    if len(reports) == 0:
        raise ValueError("no reports included")
    metadata = item["Metadata"]
    if len(metadata) == 0:
        raise ValueError("no metadata included")

    for report in reports:
        for key in ["Checksum", "UpdateState", "Metadata"]:
            if key not in report:
                raise ValueError(f"invalid data, expected {key}")
            if report[key] is None:
                raise ValueError(f"missing data, expected {key}")

        # flattern the report including the per-machine and per-report metadata
        data = metadata
        for key in report:
            # don't store some data
            if key in [
                "Created",
                "Modified",
                "BootTime",
                "UpdateState",
                "DeviceId",
                "UpdateState",
                "DeviceId",
                "Checksum",
            ]:
                continue
            if not _report_key_valid(key):
                continue
            if key == "Metadata":
                md = report[key]
                for md_key in md:
                    if not _report_key_valid(md_key):
                        continue
                    data[md_key] = md[md_key]
                continue

            # do not use deprecated names
            key_safe = REPORT_ATTR_MAP.get(key, key)

            # allow array of strings for any of the keys
            if isinstance(report[key], list):
                data[key_safe] = ",".join(report[key])
            else:
                data[key_safe] = report[key]

        # save flags
        try:
            if vercmp(data["VersionNew"], data["VersionOld"]) > 0:
                data["ReleaseFlags"] = str(FirmwareReleaseFlag.IS_UPGRADE)
        except KeyError as e:
            raise ValueError("missing data VersionNew or VersionOld") from e

        # try to find the checksum (which might not exist on this server)
        fw = (
            db.session.query(Firmware)
            .join(FirmwareRevision)
            .filter(FirmwareRevision.checksum_sha1 == report["Checksum"])
            .with_for_update(of=Firmware)
            .first()
        )
        if not fw:
            fw = (
                db.session.query(Firmware)
                .join(FirmwareRevision)
                .filter(FirmwareRevision.checksum_sha256 == report["Checksum"])
                .with_for_update(of=Firmware)
                .first()
            )
        if not fw:
            msgs.append(
                f"{report['Checksum']} did not match any known firmware archive"
            )
            continue

        # update the device checksums if there is only one component
        if (
            user
            and user.check_acl("@qa")
            and "ChecksumDevice" in data
            and len(fw.mds) == 1
        ):
            md = fw.md_prio
            found = False

            # fwupd v1.2.6 sends an array of strings, before that just a string
            checksums_maybelist = data["ChecksumDevice"]
            if isinstance(checksums_maybelist, list):
                checksums = checksums_maybelist
            else:
                checksums = [checksums_maybelist]

            # does the submitted checksum already exist as a device checksum
            for checksum in checksums:
                for csum in md.device_checksums:
                    if csum.value == checksum:
                        found = True
                        break
                if found:
                    continue
                if _is_sha1(checksum):
                    md.device_checksums.append(
                        ComponentChecksum(value=checksum, kind="SHA1")
                    )
                elif _is_sha256(checksum):
                    md.device_checksums.append(
                        ComponentChecksum(value=checksum, kind="SHA256")
                    )
                elif _is_sha384(checksum):
                    md.device_checksums.append(
                        ComponentChecksum(value=checksum, kind="SHA384")
                    )

        # find any matching report
        issue_id = 0
        if report["UpdateState"] == 3:
            issue = _find_issue_for_report_data(data, fw)
            if issue:
                issue.mtime = datetime.datetime.utcnow()
                issue_id = issue.issue_id
                msgs.append("The failure is a known issue")
                uris.append(issue.url)

        # update any old report
        r = (
            db.session.query(Report)
            .filter(Report.checksum == report["Checksum"])
            .filter(Report.machine_id == machine_id)
            .with_for_update(of=Report)
            .first()
        )
        if r:
            r.timestamp = datetime.datetime.utcnow()
            r.state = report["UpdateState"]
            r.user_id = None
            for attr in r.attributes:
                db.session.delete(attr)
            msgs.append(f"{r.checksum} replaces old report")
        else:
            # save a new report in the database
            r = Report(
                addr=_get_client_address(),
                machine_id=machine_id,
                fw=fw,
                issue_id=issue_id,
                state=report["UpdateState"],
                checksum=report["Checksum"],
            )
            msgs.append(f"{r.checksum} created as new report")

        # update the firmware so that the QA user does not have to wait 24h
        if r.state == 2:
            fw.report_success_cnt += 1
        elif r.state == 3:
            if r.issue_id:
                fw.report_issue_cnt += 1
            else:
                fw.report_failure_cnt += 1

        # update the LVFS user
        if user:
            r.user_id = user.user_id

        # save all the report entries
        for key in data:
            r.attributes.append(ReportAttribute(key=key, value=data[key]))
        db.session.add(r)
        db.session.commit()

        # does the UEFI report indicate that state has changed
        protocol_attr: Optional[ReportAttribute] = r.get_attribute_by_key("Protocol")
        if protocol_attr and protocol_attr.value == "org.uefi.capsule":
            _ensure_system_integrity_test_failure(r)

    # all done
    db.session.commit()
    return (msgs, uris)


def _report_key_valid(key: str) -> bool:
    if key.startswith("CompileVersion"):
        return True
    if key.startswith("RuntimeVersion"):
        return True
    return key.find(".") == -1


def _demote_back_to_testing(fw: Firmware, task: Task) -> None:
    # only on the production instance
    if app.config["RELEASE"] != "production":
        return

    # from the server admin
    user = db.session.query(User).filter(User.username == "anon@fwupd.org").first()
    if not user:
        return

    # send email to uploading user
    if fw.user.get_action("notify-demote-failures"):
        send_email_sync(
            "[LVFS] Firmware has been demoted",
            fw.user.email_address,
            render_template("email-firmware-demote.txt", user=fw.user, fw=fw),
            task=task,
        )

    # asynchronously sign straight away, even public remotes
    remote = db.session.query(Remote).filter(Remote.name == "testing").first()
    fw.events.append(
        FirmwareEvent(
            kind=FirmwareEventKind.DEMOTED.value,
            remote_old=fw.remote,
            remote=remote,
            user=user,
        )
    )
    fw.remote = remote
    fw.invalidate_metadata()
    task.add_fail(
        f"Demoted Firmware {fw.firmware_id}",
        f"Reported success {fw.success}%",
    )
    db.session.commit()


def _demote_check_reports(task: Task) -> None:

    # check the limits and demote back to testing if required
    firmware_ids: list[int] = []
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.name == "stable")
        .filter(Firmware.report_failure_cnt > 0)
        .order_by(Firmware.firmware_id.asc())
    ):
        firmware_ids.append(firmware_id)
    task.percentage_current = 0
    task.percentage_total = len(firmware_ids)
    for firmware_id in firmware_ids:
        with db.session.begin_nested():
            fw = (
                db.session.query(Firmware)
                .filter(Firmware.firmware_id == firmware_id)
                .with_for_update(of=Firmware)
                .one()
            )
            task.status = f"Checking demotion threshold of firmware #{firmware_id}"
            if fw.is_failure:
                _demote_back_to_testing(fw, task=task)
            task.percentage_current += 1
        if task.ended_ts:
            break


def task_regenerate(task: Task) -> None:
    _demote_check_reports(task)
