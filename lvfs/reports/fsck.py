#!/usr/bin/python3
#
# Copyright 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods,unused-argument,protected-access

from typing import Optional

from pkgversion import vercmp

from lvfs import db
from lvfs.firmware.models import FirmwareReleaseFlag
from lvfs.tasks.models import Task
from .models import Report, ReportAttribute, REPORT_ATTR_MAP
from .utils import _report_key_valid, _ensure_system_integrity_test_failure


def _fsck_reports_fixup_system_integrity(self: Report, task: Task) -> None:

    if not self.fw:
        return
    protocol_attr: Optional[ReportAttribute] = self.get_attribute_by_key("Protocol")
    if not protocol_attr:
        return
    if protocol_attr.value != "org.uefi.capsule":
        return
    _ensure_system_integrity_test_failure(self)


def _fsck_reports_fixup_old_attrs(self: Report, task: Task) -> None:
    # fix up report attrs
    fixed: list[str] = []
    for key, value in REPORT_ATTR_MAP.items():
        attr = self.get_attribute_by_key(key)
        if attr:
            attr.key = value
            if key not in fixed:
                fixed.append(key)

    # check is-upgrade is set
    attr_new = self.get_attribute_by_key("VersionNew")
    attr_old = self.get_attribute_by_key("VersionOld")
    attr_flags = self.get_attribute_by_key("ReleaseFlags")
    if (
        not attr_flags
        and attr_old
        and attr_new
        and vercmp(attr_new.value, attr_old.value) > 0
    ):
        self.attributes.append(
            ReportAttribute(
                key="ReleaseFlags", value=str(FirmwareReleaseFlag.IS_UPGRADE)
            )
        )
        fixed.append("ReleaseFlags")

    if fixed:
        task.add_fail(
            "Database::Reports",
            f"Fixed up {','.join(fixed)} keys",
        )


def _fsck_reports_fixup_invalid_attrs(self: Report, task: Task) -> None:
    # delete crazy values
    for attr in self.attributes:
        if not _report_key_valid(attr.key):
            task.add_fail(
                "Database::Reports",
                f"Deleted {attr.key}={attr.value}",
            )
            db.session.delete(attr)


def _fsck(self: Report, task: Task, kinds: Optional[list[str]] = None) -> None:
    if not kinds or "attrs" in kinds:
        _fsck_reports_fixup_old_attrs(self, task)
        _fsck_reports_fixup_invalid_attrs(self, task)
        _fsck_reports_fixup_system_integrity(self, task)
