#!/usr/bin/python3
#
# Copyright 2025 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=wrong-import-position

import os
import sys
import unittest

# allows us to run this from the project root
sys.path.append(os.path.realpath("."))

from lvfs.reports.models import Report, ReportAttribute


class TestReportModel(unittest.TestCase):
    def test_identical(self):

        r = Report()
        r.attributes.append(
            ReportAttribute(key="SystemIntegrityOld", value="UEFI:SecureBoot=1234")
        )
        r.attributes.append(
            ReportAttribute(key="SystemIntegrityNew", value="UEFI:SecureBoot=1234")
        )
        self.assertIsNone(r.system_integrity)

    def test_different(self):

        r = Report()
        r.attributes.append(
            ReportAttribute(key="SystemIntegrityOld", value="UEFI:SecureBoot=1234")
        )
        r.attributes.append(
            ReportAttribute(key="SystemIntegrityNew", value="UEFI:SecureBoot=XXXX")
        )
        self.assertEqual(r.system_integrity, {"UEFI:SecureBoot": ("1234", "XXXX")})

    def test_ignore_bootorder(self):

        r = Report()
        r.attributes.append(
            ReportAttribute(
                key="SystemIntegrityOld",
                value="\n".join(["UEFI:BootOrder=abcd", "UEFI:SecureBoot=1234"]),
            )
        )
        r.attributes.append(
            ReportAttribute(
                key="SystemIntegrityNew",
                value="\n".join(["UEFI:BootOrder=1234", "UEFI:SecureBoot=1234"]),
            )
        )
        self.assertIsNone(r.system_integrity)

    def test_ignore_bootentry_added(self):

        r = Report()
        r.attributes.append(
            ReportAttribute(
                key="SystemIntegrityOld",
                value="\n".join(["UEFI:BootOrder=1234"]),
            )
        )
        r.attributes.append(
            ReportAttribute(
                key="SystemIntegrityNew",
                value="\n".join(["UEFI:BootOrder=1234", "UEFI:Boot0004=abcd"]),
            )
        )
        self.assertIsNone(r.system_integrity)

    def test_ignore_bootentry_changed(self):

        r = Report()
        r.attributes.append(
            ReportAttribute(
                key="SystemIntegrityOld",
                value="\n".join(["UEFI:BootOrder=1234", "UEFI:Boot0004=abcd"]),
            )
        )
        r.attributes.append(
            ReportAttribute(
                key="SystemIntegrityNew",
                value="\n".join(["UEFI:BootOrder=1234", "UEFI:Boot0004=efgh"]),
            )
        )
        self.assertIsNone(r.system_integrity)

    def test_remove_bootentry(self):

        r = Report()
        r.attributes.append(
            ReportAttribute(
                key="SystemIntegrityOld",
                value="\n".join(["UEFI:BootOrder=1234", "UEFI:Boot0004=abcd"]),
            )
        )
        r.attributes.append(
            ReportAttribute(
                key="SystemIntegrityNew",
                value="\n".join(["UEFI:BootOrder=1234"]),
            )
        )
        self.assertEqual(r.system_integrity, {"UEFI:Boot0004": ("abcd", None)})


if __name__ == "__main__":

    from lvfs import create_app

    with create_app().app_context():
        unittest.main()
