// Copyright 2023 Richard Hughes <richard@hughsie.com>
// SPDX-License-Identifier: GPL-2.0-or-later

$('button').dblclick(function(e){
    if (!$(this).attr('data-toggle') && !$(this).attr('data-dismiss'))
        e.preventDefault();
});
