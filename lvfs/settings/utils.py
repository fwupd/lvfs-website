#!/usr/bin/python3
#
# Copyright 2024 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=unused-argument

from sqlalchemy.exc import IntegrityError

from lvfs.util import _get_settings


def _ensure_setting_defaults() -> None:

    from lvfs import db, ploader
    from .models import Setting

    # create all the plugin default keys
    settings = _get_settings()
    for plugin in ploader.get_all():
        for s in plugin.settings:
            if s.key not in settings:
                try:
                    db.session.add(Setting(key=s.key, value=s.default))
                    db.session.commit()
                except IntegrityError:
                    pass
