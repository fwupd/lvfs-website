#!/usr/bin/python3
#
# Copyright 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods,disable=singleton-comparison

from typing import Optional
import datetime

from lvfs.claims.models import Claim
from lvfs.tasks.models import Task
from lvfs.util import _get_datestr_from_datetime

from lvfs import db

from .utils import _generate_stats_for_claim
from .models import AnalyticClaim


def _fsck_analytics_claims(task: Task) -> None:

    # delete all existing
    task.status = "Deleting"
    db.session.query(AnalyticClaim).delete()
    db.session.commit()

    # set up add
    now = datetime.datetime.today() - datetime.timedelta(days=1)
    days_in_10years: int = 365 * 10
    task.status = "Adding"
    task.percentage_current = 0
    task.percentage_total = days_in_10years
    db.session.commit()

    # work through
    for _ in range(days_in_10years):
        datestr: int = _get_datestr_from_datetime(now)
        for claim in db.session.query(Claim):
            _generate_stats_for_claim(claim, datestr, task=task)
        now -= datetime.timedelta(days=1)
        task.percentage_current += 1
        if task.percentage_current % 50 == 0:
            task.status = f"Date {datestr}"
            db.session.commit()


def _fsck(task: Task, kinds: Optional[list[str]] = None) -> None:
    if not kinds or "claims" in kinds:
        _fsck_analytics_claims(task)
