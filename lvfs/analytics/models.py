#!/usr/bin/python3
#
# Copyright 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods

from enum import IntEnum

from sqlalchemy import Column, Integer, Text, ForeignKey
from sqlalchemy.orm import relationship

from lvfs import db


class Analytic(db.Model):  # type: ignore
    __tablename__ = "analytics"

    datestr = Column(Integer, primary_key=True, index=True)
    cnt = Column(Integer, default=1)

    def __repr__(self) -> str:
        return f"Analytic({self.datestr})"


class AnalyticVendor(db.Model):  # type: ignore
    __tablename__ = "analytics_vendor"

    analytic_id = Column(Integer, primary_key=True)
    datestr = Column(Integer, default=0, index=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    cnt = Column(Integer, default=0)

    vendor = relationship("Vendor", foreign_keys=[vendor_id])

    def __repr__(self) -> str:
        return f"AnalyticVendor({self.datestr},{self.vendor_id})"


class AnalyticVendorReports(db.Model):  # type: ignore
    __tablename__ = "analytics_vendor_reports"

    analytic_vendor_report_id = Column(Integer, primary_key=True)
    datestr = Column(Integer, default=0, index=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    cnt = Column(Integer, default=0)

    vendor = relationship("Vendor", foreign_keys=[vendor_id])

    def __repr__(self) -> str:
        return f"AnalyticVendorReports({self.datestr},{self.vendor_id})"


class AnalyticClaim(db.Model):  # type: ignore
    __tablename__ = "analytics_claim"

    analytic_id = Column(Integer, primary_key=True)
    datestr = Column(Integer, default=0, index=True)
    claim_id = Column(
        Integer, ForeignKey("claims.claim_id"), nullable=False, index=True
    )
    cnt = Column(Integer, default=0)

    claim = relationship("Claim", foreign_keys=[claim_id])

    def __repr__(self) -> str:
        return f"AnalyticClaim({self.datestr},{self.claim_id})"


class AnalyticFirmware(db.Model):  # type: ignore
    __tablename__ = "analytics_firmware"

    analytic_id = Column(Integer, primary_key=True)
    datestr = Column(Integer, default=0, index=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    cnt = Column(Integer, default=0, index=True)

    firmware = relationship("Firmware", foreign_keys=[firmware_id])

    def __repr__(self) -> str:
        return f"AnalyticFirmware({self.datestr},{self.firmware_id})"


class AnalyticUseragentKind(IntEnum):
    APP = 0
    FWUPD = 1
    LANG = 2
    DISTRO = 3
    COUNTRY = 4


class AnalyticUseragent(db.Model):  # type: ignore
    __tablename__ = "useragents"

    useragent_id = Column(Integer, primary_key=True)
    kind = Column(Integer, default=0, index=True)
    datestr = Column(Integer, default=0, index=True)
    value = Column(Text, default=None)
    cnt = Column(Integer, default=1)

    def __repr__(self) -> str:
        return f"AnalyticUseragent({self.kind},{self.datestr})"
