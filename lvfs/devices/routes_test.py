#!/usr/bin/python3
#
# Copyright 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=wrong-import-position

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_device_device_item(self, _app, client):

        payload = """
{
  "ReportVersion" : 2,
  "ReportType" : "device-list",
  "MachineId" : "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
  "Devices" : [
    {
      "Name" : "Baz",
      "Vendor" : "Foo Bar",
      "Plugin" : "colorhug",
      "Flags" : ["updatable"],
      "Icons" : ["drive-harddisk"],
      "VendorIds" : ["DMI:ACME"],
      "Guid" : ["dc3c73c0-91db-5b7b-90eb-fdf381f5f95d"],
      "InstanceIds" : ["NVME\\\\VEN_15B7&DEV_5002"]
    }
  ]
} """
        data = {
            "payload": payload,
        }
        rv = client.post(
            "/lvfs/firmware/report", data=data, content_type="multipart/form-data"
        )
        assert '"success": true' in rv.data.decode("utf-8"), rv.data.decode()
        assert '"adding device: Foo Bar Baz' in rv.data.decode(
            "utf-8"
        ), rv.data.decode()

        # and again, to remove
        rv = client.post(
            "/lvfs/firmware/report", data=data, content_type="multipart/form-data"
        )
        assert '"success": true' in rv.data.decode("utf-8"), rv.data.decode()
        assert (
            '"deleted device: Foo Bar Baz; adding device: Foo Bar Baz'
            in rv.data.decode("utf-8")
        ), rv.data.decode()

        self.login()
        rv = client.get("/lvfs/devices/report", follow_redirects=True)
        assert b"VEN_15B7" in rv.data, rv.data.decode()
        assert b"drive-harddisk" in rv.data, rv.data.decode()
        self.logout()

    def test_devices(self, _app, client):
        # upload to stable
        self.login()
        self.add_namespace(vendor_id=1)
        self.upload()
        self.run_task_worker()
        rv = client.post("/lvfs/firmware/1/promote/stable", follow_redirects=True)
        assert b"Moving firmware" in rv.data, rv.data.decode()
        self.run_task_worker()
        rv = client.get("/lvfs/firmware/1/target", follow_redirects=True)
        assert b">stable<" in rv.data, rv.data.decode()
        self.logout()

        rv = client.get("/lvfs/devices/")
        assert "ColorHug2 Device Update" in rv.data.decode("utf-8"), rv.data.decode()

        rv = client.get("/lvfs/devices/com.hughski.ColorHug2.firmware")
        assert "Use a quicker start-up sequence" in rv.data.decode(
            "utf-8"
        ), rv.data.decode()

        rv = client.get("/lvfs/devices/com.hughski.ColorHug2.firmware/analytics")
        assert "ChartDevice" in rv.data.decode("utf-8"), rv.data.decode()

    def test_device_status(self, _app, client):
        # upload to stable
        self.login()
        self.add_namespace(vendor_id=1)
        self.upload()
        self.run_task_worker()
        rv = client.post("/lvfs/firmware/1/promote/stable", follow_redirects=True)
        assert b"Moving firmware" in rv.data, rv.data.decode()
        self.run_task_worker()
        rv = client.get("/lvfs/firmware/1/target", follow_redirects=True)
        assert b">stable<" in rv.data, rv.data.decode()

        rv = client.get("/lvfs/devices/status")
        assert "com.hughski.ColorHug2.firmware" in rv.data.decode(
            "utf-8"
        ), rv.data.decode()
        assert "/lvfs/firmware/1" in rv.data.decode("utf-8"), rv.data.decode()
        assert "2.0.3" in rv.data.decode("utf-8"), rv.data.decode()

    def test_device_model_state(self, _app, client):
        # upload to stable
        self.login()
        self.add_namespace(vendor_id=1)
        self.upload()
        self.run_task_worker()
        rv = client.post("/lvfs/firmware/1/promote/stable", follow_redirects=True)
        assert b"Moving firmware" in rv.data, rv.data.decode()
        self.run_task_worker()
        rv = client.get("/lvfs/firmware/1/target", follow_redirects=True)
        assert b">stable<" in rv.data, rv.data.decode()

        rv = client.get("/lvfs/devices/status")
        assert "/lvfs/firmware/1" in rv.data.decode("utf-8"), rv.data.decode()
        assert b"Set as EOL" not in rv.data, rv.data.decode()

        rv = client.post("/lvfs/devices/state/1/eol", follow_redirects=True)
        assert b"Product updated" in rv.data, rv.data.decode()

        rv = client.get("/lvfs/devices/status")
        assert b"Set as end-of-life" in rv.data, rv.data.decode()


if __name__ == "__main__":
    unittest.main()
