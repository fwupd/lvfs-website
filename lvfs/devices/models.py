#!/usr/bin/python3
#
# Copyright 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods,protected-access

import datetime

from sqlalchemy import Column, Integer, Text, DateTime, ForeignKey, Boolean
from sqlalchemy.orm import relationship

from lvfs import db


class Product(db.Model):  # type: ignore
    __tablename__ = "products"

    product_id = Column(Integer, primary_key=True)
    appstream_id = Column(Text, nullable=False, index=True)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    mtime = Column(DateTime, nullable=True)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    state = Column(Text, nullable=True, index=True)

    user = relationship("User", foreign_keys=[user_id])

    STATE_ACTIVE = None
    STATE_EOL = "eol"

    @property
    def state_display(self) -> str:
        if self.state == self.STATE_ACTIVE:
            return "active"
        if self.state == self.STATE_EOL:
            return "end-of-life"
        return "unknown"

    def __repr__(self) -> str:
        return f"Product({self.product_id})"


class DeviceReport(db.Model):  # type: ignore
    __tablename__ = "device_report"

    device_report_id = Column(Integer, primary_key=True)
    machine_id = Column(Text, nullable=False, index=True)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=True)

    user = relationship("User", foreign_keys=[user_id])

    items = relationship(
        "DeviceReportItem", back_populates="report", cascade="all,delete,delete-orphan"
    )

    def __repr__(self) -> str:
        return f"DeviceReport({self.device_report_id})"


class DeviceReportInfo(db.Model):  # type: ignore
    __tablename__ = "device_report_infos"

    device_report_info_id = Column(Integer, primary_key=True)
    vendor_id = Column(Text, nullable=False, index=True)
    instance_id = Column(Text, nullable=False, unique=True, index=True)
    icon = Column(Text, nullable=True, default=None)
    cnt_supported = Column(Integer, default=None)
    cnt_unsupported = Column(Integer, default=None)

    @property
    def item(self) -> "DeviceReportItem":
        return (
            db.session.query(DeviceReportItem)
            .filter(DeviceReportItem.instance_id == self.instance_id)
            .first()
        )

    @property
    def cnt(self) -> int:
        return self.cnt_supported + self.cnt_unsupported

    def __repr__(self) -> str:
        return f"HsiReportInfo({self.instance_id})"


class DeviceReportItem(db.Model):  # type: ignore
    __tablename__ = "device_report_items"

    device_report_item_id = Column(Integer, primary_key=True)
    device_report_id = Column(
        Integer,
        ForeignKey("device_report.device_report_id"),
        nullable=False,
        index=True,
    )
    instance_id = Column(Text, nullable=False, index=True)
    vendor_id = Column(Text, nullable=False, index=True)
    plugin = Column(Text, nullable=False, index=True)
    vendor = Column(Text, nullable=False, index=True)
    name = Column(Text, nullable=False, index=True)
    supported = Column(Boolean, default=False)

    report = relationship("DeviceReport", back_populates="items")

    @property
    def info(self) -> DeviceReportInfo:
        return (
            db.session.query(DeviceReportInfo)
            .filter(DeviceReportInfo.instance_id == self.instance_id)
            .one()
        )

    def __repr__(self) -> str:
        return f"DeviceReportItem({self.device_report_item_id})"
