#!/usr/bin/python3
#
# Copyright 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Any, Optional, Tuple

from lvfs.hash import _is_sha256
from lvfs.users.models import User

from lvfs import db
from lvfs.components.models import ComponentGuid

from .models import DeviceReport, DeviceReportItem, DeviceReportInfo


def devices_process_upload_data(
    item: dict[Any, Any], user: Optional[User] = None
) -> Tuple[list[str], list[str]]:

    msgs: list[str] = []

    # check we got enough data
    for key in ["MachineId"]:
        if key not in item:
            raise ValueError(f"invalid data, expected {key}")
        if item[key] is None:
            raise ValueError(f"missing data, expected {key}")

    # add each firmware report
    machine_id = item["MachineId"]
    if not _is_sha256(machine_id):
        raise ValueError("MachineId invalid, expected SHA256")

    devices = item.get("Devices", [])
    if not devices:
        raise ValueError("no devices included")

    # delete any old report
    for rpt_item in (
        db.session.query(DeviceReportItem)
        .join(DeviceReport)
        .filter(DeviceReport.machine_id == machine_id)
    ):
        if rpt_item.info:
            if rpt_item.supported and rpt_item.info.cnt_supported > 0:
                rpt_item.info.cnt_supported -= 1
            if not rpt_item.supported and rpt_item.info.cnt_unsupported > 0:
                rpt_item.info.cnt_unsupported -= 1
        msgs.append(f"deleted device: {rpt_item.vendor} {rpt_item.name}")
    for rpt in db.session.query(DeviceReport).filter(
        DeviceReport.machine_id == machine_id
    ):
        db.session.delete(rpt)

    # create a new report
    rpt = DeviceReport(machine_id=machine_id, user=user)
    for device in devices:
        cnt_supported: int = 0
        cnt_unsupported: int = 0
        icon: Optional[str] = None
        try:

            # support new and old JSON formats
            vendor_id: Optional[str] = None
            if "VendorIds" in device:
                vendor_id = device["VendorIds"][0]
            elif "VendorId" in device:
                vendor_id = device["VendorId"].split("|")[0]
            if not vendor_id:
                continue

            # support new and old JSON formats
            instance_id: Optional[str] = None
            if "InstanceIds" in device:
                instance_id = device["InstanceIds"][0]
            elif "InstanceId" in device:
                instance_id = device["InstanceId"].split("|")[0]
            elif "Guid" in device:
                instance_id = device["Guid"][0]
            if not instance_id:
                continue

            rpt_item = DeviceReportItem(
                vendor=device.get("Vendor", "Unknown"),
                name=device["Name"],
                plugin=device["Plugin"],
                instance_id=instance_id,
                vendor_id=vendor_id,
            )

            # the client found something in the local metadata
            if "supported" in device["Flags"]:
                cnt_supported = 1
            else:
                # do any of the GUIDs exist on the LVFS as components?
                if (
                    db.session.query(ComponentGuid)
                    .filter(ComponentGuid.value in device["Guid"])
                    .first()
                ):
                    msgs.append(
                        f"fixing up device as supported: {rpt_item.vendor} {rpt_item.name}"
                    )
                    cnt_supported = 1
                else:
                    cnt_unsupported = 1

            # stored as a boolean so we can delete
            if cnt_supported > 0:
                rpt_item.supported = True
        except (KeyError, IndexError) as e:
            msgs.append(f"ignoring device: {e!s}")
            continue

        # icon is optional, default None
        try:
            icon = device["Icons"][0]
        except (KeyError, IndexError):
            pass

        # update the info, creating if required
        rpt_info = (
            db.session.query(DeviceReportInfo)
            .filter(DeviceReportInfo.instance_id == rpt_item.instance_id)
            .first()
        )
        if rpt_info:
            rpt_info.cnt_supported += cnt_supported
            rpt_info.cnt_unsupported += cnt_unsupported

            # fill in if not initially known
            if icon and not rpt_info.icon:
                rpt_info.icon = icon
        else:
            rpt_info = DeviceReportInfo(
                instance_id=rpt_item.instance_id,
                vendor_id=rpt_item.vendor_id,
                icon=icon,
                cnt_supported=cnt_supported,
                cnt_unsupported=cnt_unsupported,
            )
            db.session.add(rpt_info)

        # fix up wrong names
        if rpt_info.icon == "touchpad-disabled":
            rpt_info.icon = "auth-fingerprint"

        msgs.append(f"adding device: {rpt_item.vendor} {rpt_item.name}")
        rpt.items.append(rpt_item)
    db.session.add(rpt)

    # all done
    db.session.commit()
    return (msgs, [])
