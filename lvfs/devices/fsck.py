#!/usr/bin/python3
#
# Copyright 2024 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods,disable=singleton-comparison

from typing import Optional

from lvfs import db
from lvfs.tasks.models import Task
from .models import DeviceReport, DeviceReportItem, DeviceReportInfo


def _fsck_devices_purge(task: Task) -> None:

    db.session.query(DeviceReportItem).delete()
    db.session.query(DeviceReportInfo).delete()
    db.session.query(DeviceReport).delete()
    task.add_pass("Deleted", "All device reports")


def _fsck(task: Task, kinds: Optional[list[str]] = None) -> None:
    if not kinds or "purge" in kinds:
        _fsck_devices_purge(task)
