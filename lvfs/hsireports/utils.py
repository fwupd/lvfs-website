#!/usr/bin/python3
#
# Copyright 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Any, Optional, Tuple

from lvfs.hash import _is_sha256
from lvfs.users.models import User

from lvfs import db

from .models import HsiReport, HsiReportAttr


def hsireports_process_upload_data(
    item: dict[Any, Any], user: Optional[User] = None
) -> Tuple[list[str], list[str]]:

    msgs: list[str] = []
    uris: list[str] = []

    # check we got enough data
    for key in ["Metadata"]:
        if key not in item:
            raise ValueError(f"invalid data, expected {key}")
        if item[key] is None:
            raise ValueError(f"missing data, expected {key}")

    # add each firmware report
    machine_id = item["MachineId"]
    if not _is_sha256(machine_id):
        raise ValueError("MachineId invalid, expected SHA256")
    hsireports = item.get("HsiReports", [])
    sec_attrs = item.get("SecurityAttributes", [])
    if not hsireports and not sec_attrs:
        raise ValueError("no hsireports included")
    metadata = item["Metadata"]
    if not metadata:
        raise ValueError("no metadata included")

    if sec_attrs:
        # required metadata for this report type
        for key in ["HostProduct", "HostFamily", "HostVendor", "HostSku"]:
            if key not in metadata:
                raise ValueError(f"invalid data, expected {key}")
            if metadata[key] is None:
                raise ValueError(f"missing data, expected {key}")

        # check attrs
        for sec_attr in sec_attrs:
            for key in ["AppstreamId", "HsiResult"]:
                if key not in sec_attr:
                    raise ValueError(f"invalid data, expected {key}")
                if sec_attr[key] is None:
                    raise ValueError(f"missing data, expected {key}")

        # update any old report
        rpt = (
            db.session.query(HsiReport)
            .filter(HsiReport.machine_id == machine_id)
            .first()
        )
        if rpt:
            db.session.delete(rpt)

        # construct a single string
        distro = (
            f"{metadata.get('DistroId', 'Unknown')} "
            f"{metadata.get('DistroVersion', 'Unknown')} "
            f"({metadata.get('DistroVariant', 'Unknown')})"
        )

        # save a new report in the database
        host_security_parts = metadata["HostSecurityId"].split(" ")
        host_security_version: Optional[str] = None
        try:
            host_security_version = host_security_parts[1][2:-1]
        except IndexError:
            pass
        rpt = HsiReport(
            user=user,
            machine_id=machine_id,
            distro=distro,
            kernel_cmdline=metadata.get("KernelCmdline"),
            kernel_version=metadata.get("KernelVersion"),
            host_product=metadata["HostProduct"],
            host_vendor=metadata["HostVendor"],
            host_family=metadata["HostFamily"],
            host_sku=metadata["HostSku"],
            host_security_id=host_security_parts[0],
            host_security_version=host_security_version or "",
        )
        for sec_attr in sec_attrs:
            flags = sec_attr.get("Flags", [])
            attr = HsiReportAttr(
                appstream_id=sec_attr["AppstreamId"],
                hsi_result=sec_attr["HsiResult"],
                is_runtime="runtime-issue" in flags,
                is_success="success" in flags,
                is_obsoleted="obsoleted" in flags,
            )
            rpt.attrs.append(attr)
        db.session.add(rpt)

    # all done
    db.session.commit()
    return (msgs, uris)
