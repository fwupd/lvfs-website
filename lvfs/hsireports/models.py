#!/usr/bin/python3
#
# Copyright 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods

import datetime
from typing import Optional, Any

from flask import g

from sqlalchemy import Column, Integer, Text, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from lvfs import db

from lvfs.users.models import User


class HsiReportInfo(db.Model):  # type: ignore
    __tablename__ = "hsi_report_infos"

    hsi_report_info_id = Column(Integer, primary_key=True)
    appstream_id = Column(Text, nullable=False, unique=True, index=True)
    appstream_id_old = Column(Text, default=None, index=True)
    name = Column(Text, default=None)
    level = Column(Integer, default=None)

    def __repr__(self) -> str:
        return f"HsiReportInfo({self.appstream_id})"


class HsiReportAttr(db.Model):  # type: ignore
    __tablename__ = "hsi_report_attrs"

    report_attr_id = Column(Integer, primary_key=True)
    hsi_report_id = Column(
        Integer, ForeignKey("hsi_reports.hsi_report_id"), nullable=False, index=True
    )
    appstream_id = Column(Text, nullable=False, index=True)
    hsi_result = Column(Text, default=None)
    is_success = Column(Boolean, default=False)
    is_runtime = Column(Boolean, default=False)
    is_obsoleted = Column(Boolean, default=False)

    report = relationship("HsiReport", back_populates="attrs")

    def to_dict(self) -> dict[str, Any]:
        data: dict[str, Any] = {}

        data["AppstreamId"] = self.appstream_id
        if self.hsi_result:
            data["HsiResult"] = self.hsi_result

        # flags
        data_flags: list[str] = []
        if self.is_runtime:
            data_flags.append("runtime-issue")
        if self.is_success:
            data_flags.append("success")
        if self.is_obsoleted:
            data_flags.append("obsoleted")
        if data_flags:
            data["Flags"] = data_flags
        return data

    def __repr__(self) -> str:
        return f"HsiReportAttr({self.key},{self.value})"


class HsiReport(db.Model):  # type: ignore
    __tablename__ = "hsi_reports"

    hsi_report_id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    machine_id = Column(String(64), nullable=False)
    distro = Column(Text, default=None)
    kernel_cmdline = Column(Text, default=None)
    kernel_version = Column(Text, default=None)
    host_product = Column(Text, default=None, index=True)
    host_vendor = Column(Text, default=None, index=True)
    host_family = Column(Text, default=None, index=True)
    host_sku = Column(Text, default=None, index=True)
    host_security_id = Column(Text, nullable=False)
    host_security_version = Column(Text, nullable=False)
    user_id = Column(Integer, ForeignKey("users.user_id"), default=None)

    user = relationship("User", foreign_keys=[user_id])
    attrs = relationship(
        "HsiReportAttr", back_populates="report", cascade="all,delete,delete-orphan"
    )

    def to_dict(self) -> dict[str, Any]:
        data: dict[str, Any] = {}

        data["Timestamp"] = self.timestamp
        data["MachineId"] = self.machine_id
        data["HostSecurityId"] = self.host_security_id
        data["HostSecurityVersion"] = self.host_security_version
        if self.distro:
            data["Distro"] = self.distro
        if self.kernel_cmdline:
            data["KernelCmdline"] = self.kernel_cmdline
        if self.kernel_version:
            data["KernelVersion"] = self.kernel_version
        if self.host_product:
            data["HostProduct"] = self.host_product
        if self.host_vendor:
            data["HostVendor"] = self.host_vendor
        if self.host_family:
            data["HostFamily"] = self.host_family
        if self.host_sku:
            data["HostSku"] = self.host_sku

        # attrs
        data_attrs: list[dict[str, Any]] = []
        for attr in self.attrs:
            data_attrs.append(attr.to_dict())
        if data_attrs:
            data["SecurityAttributes"] = data_attrs
        return data

    @property
    def host_sku_valid(self) -> bool:
        return self.host_sku not in [
            None,
            "",
            "Default string",
            "None",
            "To be filled by O.E.M.",
        ]

    @property
    def host_vendor_valid(self) -> bool:
        return self.host_vendor not in [
            None,
            "",
            "Default string",
            "None",
            "System manufacturer",
            "To Be Filled By O.E.M.",
            "Unknown",
        ]

    @property
    def host_product_valid(self) -> bool:
        return self.host_product not in [
            None,
            "",
            "Default string",
            "None",
            "System Product Name",
            "To be filled by O.E.M.",
        ]

    @property
    def host_family_valid(self) -> bool:
        return self.host_family not in [
            None,
            "",
            "*",
            "BSW",
            "CML",
            "Default string",
            "Desktop",
            "HSW",
            "KBL",
            "N/A",
            "None",
            "Not Applicable",
            "Notebook",
            "Raspberry Pi",
            "SKL",
            "TGL",
            "Surface",
            "To be filled by O.E.M.",
        ]

    @property
    def title(self) -> str:
        parts: list[str] = []
        if self.host_vendor_valid:
            parts.append(self.host_vendor)
        if self.host_product_valid:
            parts.append(self.host_product)
        if self.host_family_valid:
            parts.append(self.host_family)
        return " ".join(parts)

    def check_acl(self, action: str, user: Optional[User] = None) -> bool:
        # fall back
        if not user:
            user = g.user
        if not user:
            return False
        if user.check_acl("@admin"):
            return True

        # lockdown
        if g.in_lockdown:
            return False

        # depends on the action requested
        if action == "@delete":
            return False
        if action == "@view":
            return True
        raise NotImplementedError(f"unknown security check action: {self}:{action}")

    def __repr__(self) -> str:
        return f"HsiReport({self.hsi_report_id})"
