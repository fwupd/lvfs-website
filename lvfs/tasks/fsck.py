#!/usr/bin/python3
#
# Copyright 2024 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods,disable=singleton-comparison,protected-access

from typing import Optional

from lvfs.tasks.models import Task

from lvfs import db


def _fsck_task_success(task: Task) -> None:

    cnt: int = 0
    for other_task in db.session.query(Task).filter(Task.success == None):
        other_task.success = other_task._success
        cnt += 1
    task.add_pass(f"Fixed {cnt} tasks")
    db.session.commit()


def _fsck(task: Task, kinds: Optional[list[str]] = None) -> None:
    if not kinds or "success" in kinds:
        _fsck_task_success(task)
