#!/usr/bin/python3
#
# Copyright 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Any

from flask import Blueprint, request, url_for, redirect, flash, render_template, g
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.util import admin_login_required

from .models import License, LicenseAlias

bp_licenses = Blueprint("licenses", __name__, template_folder="templates")


@bp_licenses.route("/")
@login_required
@admin_login_required
def route_list() -> Any:
    licenses = db.session.query(License).order_by(License.value.asc()).all()
    return render_template("license-list.html", category="admin", licenses=licenses)


@bp_licenses.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:
    # ensure has enough data
    try:
        value = request.form["value"]
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("licenses.route_list"))
    if not value or value.find(" ") != -1:
        flash("Failed to add license: Value is not valid", "warning")
        return redirect(url_for("licenses.route_list"))

    # add license
    try:
        lic = License(value=request.form["value"])
        db.session.add(lic)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add license: The license already exists", "info")
        return redirect(url_for("licenses.route_list"))
    flash("Added license", "info")
    return redirect(url_for("licenses.route_show", license_id=lic.license_id))


@bp_licenses.post("/<int:license_id>/delete")
@login_required
@admin_login_required
def route_delete(license_id: int) -> Any:
    # get license
    try:
        db.session.query(License).filter(License.license_id == license_id).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("License cannot be deleted", "info")
        return redirect(url_for("licenses.route_list"))
    flash("Deleted license", "info")
    return redirect(url_for("licenses.route_list"))


@bp_licenses.post("/<int:license_id>/modify")
@login_required
@admin_login_required
def route_modify(license_id: int) -> Any:
    # find license
    try:
        lic = (
            db.session.query(License)
            .filter(License.license_id == license_id)
            .with_for_update(of=License)
            .one()
        )
    except NoResultFound:
        flash("No license found", "info")
        return redirect(url_for("licenses.route_list"))

    # modify license
    lic.is_content = bool("is_content" in request.form)
    lic.is_approved = bool("is_approved" in request.form)
    lic.requires_source = bool("requires_source" in request.form)
    for key in ["name", "text", "url"]:
        if key in request.form:
            setattr(lic, key, request.form[key] or None)
    db.session.commit()

    # success
    flash("Modified license", "info")
    return redirect(url_for("licenses.route_show", license_id=license_id))


@bp_licenses.route("/<int:license_id>")
@login_required
@admin_login_required
def route_show(license_id: int) -> Any:
    # find license
    try:
        lic = db.session.query(License).filter(License.license_id == license_id).one()
    except NoResultFound:
        flash("No license found", "info")
        return redirect(url_for("licenses.route_list"))

    # show details
    return render_template("license-details.html", category="admin", lic=lic)


@bp_licenses.route("/<int:license_id>/alias/list")
@login_required
@admin_login_required
def route_alias_list(license_id: int) -> Any:

    try:
        lic = db.session.query(License).filter(License.license_id == license_id).one()
    except NoResultFound:
        flash("No license found", "info")
        return redirect(url_for("licenses.route_list"))
    return render_template(
        "license-alias-list.html",
        category="admin",
        lic=lic,
    )


@bp_licenses.post("/<int:license_id>/alias/create")
@login_required
@admin_login_required
def route_alias_create(license_id: int) -> Any:
    """Add a license alias"""
    try:
        value = request.form["value"]
        if value.find(" ") != -1:
            flash("Invalid alias value", "warning")
            return redirect(url_for("licenses.route_alias_list", license_id=license_id))
        lic = (
            db.session.query(License)
            .filter(License.license_id == license_id)
            .with_for_update(of=License)
            .one()
        )
        alias = LicenseAlias(value=value, user=g.user)
        lic.alias_map.append(alias)
        db.session.commit()
    except KeyError:
        flash("No form value", "warning")
        return redirect(url_for("licenses.route_alias_list", license_id=license_id))
    except NoResultFound:
        flash("Failed to get license details", "warning")
        return redirect(url_for("licenses.route_alias_list", license_id=license_id))
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add license alias: already exists", "info")
        return redirect(url_for("licenses.route_alias_list", license_id=license_id))
    flash("Added alias value", "info")
    return redirect(
        url_for(
            "licenses.route_alias_list",
            license_id=license_id,
        )
    )


@bp_licenses.post("/<int:license_id>/alias/<int:license_alias_id>/delete")
@login_required
@admin_login_required
def route_alias_delete(license_id: int, license_alias_id: int) -> Any:
    """Delete a license alias"""
    try:
        lic_alias = (
            db.session.query(LicenseAlias)
            .filter(LicenseAlias.license_alias_id == license_alias_id)
            .join(License)
            .filter(License.license_id == license_id)
            .with_for_update(of=License)
            .one()
        )
        db.session.delete(lic_alias)
        db.session.commit()
    except NoResultFound:
        flash("Failed to get license", "warning")
        return redirect(url_for("licenses.route_list"))
    except IntegrityError:
        db.session.rollback()
        flash("Failed to remove license alias", "info")
        return redirect(url_for("licenses.route_list"))
    flash("Deleted alias value", "info")
    return redirect(
        url_for("licenses.route_alias_list", license_id=lic_alias.license_id)
    )
