#!/usr/bin/python3
#
# Copyright 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods

import datetime

from sqlalchemy import (
    Column,
    Integer,
    Text,
    Boolean,
    DateTime,
    ForeignKey,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship

from lvfs import db


class LicenseAlias(db.Model):  # type: ignore
    __tablename__ = "license_alias"

    license_alias_id = Column(Integer, primary_key=True)
    license_id = Column(
        Integer, ForeignKey("licenses.license_id"), nullable=False, index=True
    )
    value = Column(Text, nullable=False, unique=True)  # 'CC0-1.0'
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

    lic = relationship("License")
    user = relationship("User", foreign_keys=[user_id])

    __table_args__ = (
        UniqueConstraint(
            "license_id",
            "value",
            name="uq_component_alias_license_id_value",
        ),
    )

    def __repr__(self) -> str:
        return f"LicenseAlias({self.license_alias_id},{self.license_id})"


class License(db.Model):  # type: ignore
    __tablename__ = "licenses"

    license_id = Column(Integer, primary_key=True)
    value = Column(Text, nullable=False, unique=True)  # 'CC0-1.0'
    name = Column(Text, default=None)  # 'Creative Commons Zero'
    text = Column(Text, default=None)  # 'THE LICENSE TEXT'
    is_content = Column(Boolean, default=False)
    is_approved = Column(Boolean, default=False)
    requires_source = Column(Boolean, default=False)
    url = Column(Text, default=None)
    ctime = Column(DateTime, default=datetime.datetime.utcnow)

    alias_map = relationship(
        "LicenseAlias", back_populates="lic", cascade="all,delete,delete-orphan"
    )

    def __repr__(self) -> str:
        return f"License({self.license_id},{self.value})"
