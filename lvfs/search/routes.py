#!/usr/bin/python3
#
# Copyright 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import fnmatch
import datetime
from typing import Any

from flask import current_app as app
from flask import (
    Blueprint,
    request,
    render_template,
    flash,
    redirect,
    url_for,
    Response,
)
from flask_login import login_required

from sqlalchemy import func, or_
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.components.models import (
    Component,
    ComponentGuid,
    ComponentTag,
    ComponentIssue,
    ComponentKeyword,
    ComponentRequirement,
)
from lvfs.clients.models import ClientAcl
from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.metadata.models import Remote
from lvfs.util import (
    admin_login_required,
    _get_client_address,
    _split_search_string,
    _validate_guid,
)
from lvfs.vendors.models import Vendor
from lvfs.users.models import User

from .models import SearchEvent

bp_search = Blueprint("search", __name__, template_folder="templates")


@bp_search.post("/<int:search_event_id>/delete")
@login_required
@admin_login_required
def route_delete(search_event_id: int) -> Any:
    try:
        db.session.query(SearchEvent).filter(
            SearchEvent.search_event_id == search_event_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("search.route_search"))
    flash("Deleted search event", "info")
    return redirect(url_for("analytics.route_search_history"))


def _add_search_event(ev: SearchEvent) -> None:
    if (
        db.session.query(SearchEvent)
        .filter(SearchEvent.value == ev.value)
        .filter(SearchEvent.addr == ev.addr)
        .first()
    ):
        return
    db.session.add(ev)
    db.session.commit()


@bp_search.route("/firmware", methods=["GET", "POST"])
@bp_search.route("/firmware/<int:max_results>", methods=["GET", "POST"])
@login_required
def route_fw(max_results: int = 100) -> Any:
    if "value" not in request.args:
        flash("No search value!", "danger")
        return redirect(url_for("search.route_search"))
    keywords_unsafe = _split_search_string(request.args["value"])
    if not keywords_unsafe:
        keywords_unsafe = request.args["value"].split(" ")

    # never allow empty keywords
    keywords: list[str] = []
    for keyword in keywords_unsafe:
        if keyword:
            keywords.append(keyword)
    if not keywords:
        flash("No valid search value!", "danger")
        return redirect(url_for("search.route_search"))

    # use keywords first
    fws = (
        db.session.query(Firmware)
        .join(Component)
        .join(ComponentKeyword)
        .filter(ComponentKeyword.value.in_(keywords))
        .order_by(Firmware.timestamp.desc())
        .limit(max_results)
        .all()
    )

    # try GUIDs
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentGuid)
            .filter(ComponentGuid.value.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try tags
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentTag)
            .filter(ComponentTag.value.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try version numbers
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .filter(Component.version.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try appstream ID
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .filter(Component.appstream_id.startswith(keywords[0]))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try CVE, e.g. CVE-2018-3646
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentIssue)
            .filter(func.lower(ComponentIssue.value).in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try filename (with hash)
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(FirmwareRevision)
            .filter(FirmwareRevision.filename.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try checksums
    if not fws:
        fws = (
            db.session.query(Firmware)
            .filter(
                or_(
                    Firmware.checksum_upload_sha1.in_(keywords),
                    Firmware.checksum_upload_sha256.in_(keywords),
                )
            )
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(FirmwareRevision)
            .filter(
                or_(
                    FirmwareRevision.checksum_sha1.in_(keywords),
                    FirmwareRevision.checksum_sha256.in_(keywords),
                )
            )
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try requirements
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentRequirement)
            .filter(ComponentRequirement.value.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # filter by ACL
    fws_safe: list[Firmware] = []
    for fw in fws:
        if fw.check_acl("@view"):
            fws_safe.append(fw)

    return render_template(
        "firmware-search.html",
        category="firmware",
        state="search",
        remote=None,
        fws=fws_safe,
    )


def _is_security_scanner(value: str) -> bool:

    for key in [
        "${${",
        "ALL_USERS",
        "BENCHMARK",
        "DBMS_PIPE",
        "DOCTYPE",
        "DROP FUNCTION",
        "EXTRACTVALUE",
        "GENERATE_SERIES",
        "INFORMATION_SCHEMA",
        "MAKE_SET",
        "PG_SLEEP",
        "RANDOMBLOB",
        "SELECT COUNT",
        "SLEEP(5)",
        "SYSIBM",
        "SYSMASTER",
        "SYSTEMDRIVE",
        "SYSTEMROOT",
        "WAITFOR DELAY",
    ]:
        if value.find(key) != -1:
            return True
    return False


@bp_search.route("", methods=["GET", "POST"])
@bp_search.post("/<int:max_results>")
def route_search(max_results: int = 150) -> Any:
    # no search results
    if "value" not in request.args:
        return render_template(
            "search.html", mds=None, search_size=-1, keywords_good=[], keywords_bad=[]
        )

    # ban the robots that ignore robots.txt
    user_agent = request.headers.get("User-Agent")
    if user_agent:
        for pattern in app.config.get("BANNED_USERAGENTS", []):
            if fnmatch.fnmatch(user_agent, pattern):
                return Response(
                    response="user agent banned", status=403, mimetype="text/plain"
                )

    # check client address against blocklist
    client_address = _get_client_address()
    try:
        cacl = (
            db.session.query(ClientAcl).filter(ClientAcl.addr == client_address).one()
        )
        if cacl.cnt < 50000:
            cacl.mtime = datetime.datetime.utcnow()
            cacl.cnt = cacl.cnt + 1 if cacl.cnt else 1
            cacl.resource = request.args["value"]
            cacl.user_agent = user_agent
            db.session.commit()
        return Response(
            response=f"{cacl.message}\n",
            status=429,
            mimetype="text/plain",
        )
    except NoResultFound:
        pass

    # the client is doing something dumb
    if _is_security_scanner(request.args["value"]):
        try:
            cacl = ClientAcl(
                addr=client_address,
                message="Client blocked due to search term abuse.",
                user_id=User.ID_ANON,
                user_agent=user_agent,
                resource=request.args["value"],
            )
            db.session.add(cacl)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
        else:
            return Response(
                response=cacl.message,
                status=429,
                mimetype="text/plain",
            )

    # sanity check
    value: str = request.args["value"].strip()
    if value.find("\0") != -1:
        flash("Not a valid search string!", "danger")
        return redirect(url_for("search.route_search"))

    mds: list[Component] = []
    appstream_ids: list[str] = []
    vendors: list[Vendor] = []

    # a GUID
    if _validate_guid(value):
        for md in sorted(
            db.session.query(Component)
            .join(ComponentGuid)
            .filter(ComponentGuid.value == value)
            .join(Firmware)
            .join(Remote)
            .filter(Remote.is_public)
            .limit(max_results * 4),
            reverse=True,
        ):
            if md.appstream_id in appstream_ids:
                continue
            mds.append(md)
            appstream_ids.append(md.appstream_id)
            if md.fw.vendor not in vendors:
                vendors.append(md.fw.vendor)
        return render_template(
            "search.html",
            mds=mds[:max_results],
            search_size=len(mds),
            vendors=vendors,
            keywords_good=[value],
            keywords_bad=[],
        )

    # components that match
    keywords = _split_search_string(value)
    ids = (
        db.session.query(ComponentKeyword.component_id)
        .filter(ComponentKeyword.value.in_(keywords))
        .group_by(ComponentKeyword.component_id)
        .having(func.count() == len(keywords))
        .subquery()
    )
    for md in sorted(
        db.session.query(Component)
        .join(ids)
        .join(Firmware)
        .join(Remote)
        .filter(Remote.is_public)
        .limit(max_results * 4),
        reverse=True,
    ):
        if md.appstream_id in appstream_ids:
            continue
        mds.append(md)
        appstream_ids.append(md.appstream_id)
        if md.fw.vendor not in vendors:
            vendors.append(md.fw.vendor)

    # get any vendor information as a fallback
    keywords_good: list[str] = []
    keywords_bad: list[str] = []
    if mds:
        keywords_good.extend(keywords)
        search_method = "FW"
    else:
        search_method = "AND"

    # always add vendor results
    for vendor in db.session.query(Vendor).filter(Vendor.visible_for_search):
        for kw in keywords:
            if vendor.keywords:
                if kw in vendor.keywords:
                    if vendor not in vendors:
                        vendors.append(vendor)
                    if kw not in keywords_good:
                        keywords_good.append(kw)
                    break
            if vendor.display_name:
                if kw in _split_search_string(vendor.display_name):
                    if vendor not in vendors:
                        vendors.append(vendor)
                    if kw not in keywords_good:
                        keywords_good.append(kw)
                    break
    for kw in keywords:
        if kw not in keywords_good:
            keywords_bad.append(kw)

    # this seems like we're over-logging but I'd like to see how people are
    # searching so we can tweak the algorithm used
    _add_search_event(
        SearchEvent(
            value=value,
            addr=client_address,
            count=len(mds) + len(vendors),
            method=search_method,
        )
    )

    return render_template(
        "search.html",
        mds=mds[:max_results],
        search_size=len(mds),
        vendors=vendors,
        keywords_good=keywords_good,
        keywords_bad=keywords_bad,
    )
