#!/usr/bin/python3
#
# Copyright 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import json
from flask import g
from flask import current_app as app
from flask_mail import Message

from lvfs import mail, db
from lvfs.tasks.models import Task


def send_email_sync(subject: str, recipient: str, text_body: str, task: Task) -> None:
    if app.config.get("MAIL_SUPPRESS_SEND"):
        if app.config.get("DEBUG"):
            # also save the email *contents* -- which could be password...
            task.add_fail(f"Not sending email to {recipient}", text_body)
        else:
            task.add_fail(f"Not sending email to {recipient}")
        return
    task.add_pass(f"Sending email to {recipient}")
    msg = Message(subject, recipients=[recipient])
    msg.body = text_body
    mail.send(msg)


def task_send_email(task: Task) -> None:
    values = json.loads(task.value)
    send_email_sync(
        values["subject"], values["recipient"], values["text_body"], task=task
    )


def send_email(subject: str, recipient: str, text_body: str) -> None:
    if hasattr(g, "user"):
        user_id = g.user.user_id
    else:
        user_id = 2
    db.session.add(
        Task(
            value=json.dumps(
                {"subject": subject, "recipient": recipient, "text_body": text_body}
            ),
            caller=__name__,
            user_id=user_id,
            function="lvfs.emails.task_send_email",
        )
    )
    db.session.commit()
