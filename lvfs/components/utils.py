#!/usr/bin/python3
#
# Copyright 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import io
import socket
from zipfile import ZipFile, ZIP_DEFLATED

from flask import g
from uswid import (
    uSwidContainer,
    uSwidEntity,
    uSwidEntityRole,
    uSwidEvidence,
    uSwidFormatCycloneDX,
    uSwidFormatSpdx,
    uSwidFormatSwid,
    uSwidComponent,
    uSwidLink,
    uSwidLinkRel,
)

from lvfs.components.models import Component

COMPONENT_SWID_REL_TO_DISPLAY = {
    uSwidLinkRel.ANCESTOR: "Ancestor",
    uSwidLinkRel.COMPILER: "Compiler",
    uSwidLinkRel.COMPONENT: "Component",
    uSwidLinkRel.FEATURE: "Feature",
    uSwidLinkRel.INSTALLATION_MEDIA: "Installation Media",
    uSwidLinkRel.LICENSE: "License",
    uSwidLinkRel.PACKAGE_INSTALLER: "Package Installer",
    uSwidLinkRel.PARENT: "Parent",
    uSwidLinkRel.PATCHES: "Patches",
    uSwidLinkRel.REQUIRES: "Requires",
    uSwidLinkRel.SEE_ALSO: "See Also",
    uSwidLinkRel.SUPERSEDES: "Supersedes",
    uSwidLinkRel.SUPPLEMENTAL: "Supplemental",
}


def _build_swid_component_root(md: Component) -> uSwidComponent:
    """create component for the index with original vendor and LVFS"""

    component = uSwidComponent(
        tag_id=md.appstream_id,
        software_name=md.name,
        software_version=md.version_display,
        generator="LVFS",
    )
    component.add_entity(
        uSwidEntity(
            name="LVFS",
            regid=g.host_name,
            roles=[uSwidEntityRole.TAG_CREATOR, uSwidEntityRole.DISTRIBUTOR],
        )
    )
    component.add_entity(
        uSwidEntity(
            name=md.fw.vendor.display_name,
            regid=md.fw.vendor.regid,
            roles=[uSwidEntityRole.SOFTWARE_CREATOR],
        )
    )
    component.add_entity(
        uSwidEntity(
            name=md.fw.vendor.display_name,
            regid=md.fw.vendor.regid,
            roles=[uSwidEntityRole.SOFTWARE_CREATOR],
        )
    )
    component.add_evidence(uSwidEvidence(md.fw.timestamp, socket.getfqdn()))
    for swid in md.swids:
        component.add_link(
            uSwidLink(rel=uSwidLinkRel.COMPONENT, href=f"swid:{swid.tag_id}")
        )
    return component


def _build_swid_archive(md: Component) -> bytes:
    buf = io.BytesIO()
    component: uSwidComponent = _build_swid_component_root(md)
    with ZipFile(buf, mode="a", compression=ZIP_DEFLATED, allowZip64=False) as zf:
        zf.writestr("index.xml", uSwidFormatSwid().save(uSwidContainer([component])))
        for swid in md.swids:
            zf.writestr(f"{swid.tag_id}.xml", swid.value)

        # mark the files as having been created on Windows
        for zfile in zf.filelist:
            zfile.create_system = 0

    return buf.getvalue()


def _build_swid_cyclonedx(md: Component) -> bytes:
    # create export
    container: uSwidContainer = uSwidContainer([_build_swid_component_root(md)])
    for swid in md.swids:
        for component in uSwidFormatSwid().load(swid.value.encode()):
            container.append(component)
    return uSwidFormatCycloneDX().save(container)


def _build_swid_spdx(md: Component) -> bytes:
    # create export
    container: uSwidContainer = uSwidContainer([_build_swid_component_root(md)])
    for swid in md.swids:
        for component in uSwidFormatSwid().load(swid.value.encode()):
            container.append(component)
    return uSwidFormatSpdx().save(container)
