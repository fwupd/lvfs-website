#!/usr/bin/python3
#
# Copyright 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Any

from flask import Blueprint, request, url_for, redirect, flash, render_template
from flask_login import login_required
from sqlalchemy.exc import IntegrityError, NoResultFound

from lvfs import db

from lvfs.util import admin_login_required
from lvfs.util import DEVICE_ICONS

from .models import Category

bp_categories = Blueprint("categories", __name__, template_folder="templates")


@bp_categories.route("/")
@login_required
@admin_login_required
def route_list() -> Any:
    categories = db.session.query(Category).order_by(Category.category_id.asc()).all()
    return render_template(
        "category-list.html", category="admin", categories=categories
    )


@bp_categories.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:
    try:
        value = request.form["value"] or None
    except KeyError:
        flash("No form data found", "warning")
        return redirect(url_for("categories.route_list"))
    if not value or not value.startswith("X-") or value.find(" ") != -1:
        flash("Failed to add category: Value needs to be a valid group name", "warning")
        return redirect(url_for("categories.route_list"))

    # add category
    try:
        cat = Category(value=request.form["value"])
        db.session.add(cat)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add category: The category already exists", "info")
        return redirect(url_for("categories.route_list"))
    flash("Added category", "info")
    return redirect(url_for("categories.route_show", category_id=cat.category_id))


@bp_categories.post("/<int:category_id>/delete")
@login_required
@admin_login_required
def route_delete(category_id: int) -> Any:
    # delete
    try:
        db.session.query(Category).filter(Category.category_id == category_id).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Category still in use", "warning")
        return redirect(url_for("categories.route_show", category_id=category_id))

    flash("Deleted category", "info")
    return redirect(url_for("categories.route_list"))


@bp_categories.post("/<int:category_id>/modify")
@login_required
@admin_login_required
def route_modify(category_id: int) -> Any:
    # find category
    try:
        cat = (
            db.session.query(Category)
            .filter(Category.category_id == category_id)
            .with_for_update(of=Category)
            .one()
        )
    except NoResultFound:
        flash("No category found", "info")
        return redirect(url_for("categories.route_list"))

    # modify category
    cat.expect_device_checksum = bool("expect_device_checksum" in request.form)
    for key in ["name", "icon", "fallback_id"]:
        if key in request.form:
            setattr(cat, key, request.form[key] or None)
    db.session.commit()

    # success
    flash("Modified category", "info")
    return redirect(url_for("categories.route_show", category_id=category_id))


@bp_categories.route("/<int:category_id>")
@login_required
@admin_login_required
def route_show(category_id: int) -> Any:
    # find category
    try:
        cat = (
            db.session.query(Category).filter(Category.category_id == category_id).one()
        )
    except NoResultFound:
        flash("No category found", "info")
        return redirect(url_for("categories.route_list"))

    # show details
    categories = db.session.query(Category).order_by(Category.name.asc()).all()
    return render_template(
        "category-details.html",
        categories=categories,
        icons=DEVICE_ICONS,
        category="admin",
        cat=cat,
    )
