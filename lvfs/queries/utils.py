#!/usr/bin/python3
#
# Copyright 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Optional

import yara

from lvfs import db

from lvfs.components.models import Component, ComponentShard
from lvfs.firmware.models import Firmware
from lvfs.metadata.models import Remote
from lvfs.tasks.models import Task

from .models import YaraQuery, YaraQueryResult


def task_query_run(task: Task) -> None:
    query = (
        db.session.query(YaraQuery).filter(YaraQuery.yara_query_id == task.id).first()
    )
    if not query:
        return
    _query_run(query, task=task)


def _query_run_shard_yara(
    query: YaraQuery, md: Component, shard: ComponentShard
) -> None:
    blob: Optional[bytes] = shard.read()
    if not blob:
        return
    matches = query.rules.match(data=blob)
    for match in matches:
        msg = match.rule
        for string in match.strings:
            if len(string) == 3:
                try:
                    msg += f": found {string[2].decode()}"
                except UnicodeDecodeError:
                    pass
        query.results.append(YaraQueryResult(md=md, shard=shard, result=msg))


def _query_run_component_yara(query: YaraQuery, md: Component) -> None:

    blob = md.read()
    if not blob:
        return
    matches = query.rules.match(data=blob)
    for match in matches:
        msg = match.rule
        for string in match.strings:
            if len(string) == 3:
                try:
                    msg += f": found {string[2].decode()}"
                except UnicodeDecodeError:
                    pass
        query.results.append(YaraQueryResult(md=md, result=msg))

    # one more thing scanned
    query.total += 1


def _query_setup_yara(query: YaraQuery) -> None:
    try:
        query.component_content = True
        query.rules = yara.compile(source=query.value)
    except yara.SyntaxError as e:
        raise NotImplementedError(f"Failed to compile rules: {e!s}") from e
    for line in query.value.replace("{", "\n").split("\n"):
        if line.startswith("rule "):
            query.title = line[5:]
            break
    db.session.commit()


def _query_run(query: YaraQuery, task: Task) -> None:
    try:
        _query_setup_yara(query)
    except NotImplementedError as e:
        query.error = str(e)
        db.session.commit()
        return

    # restrict these to specific GUIDs
    for line in query.value.split("\n"):
        idx = line.find("X-Volume-GUIDs")
        if idx != -1:
            for guid in line[idx + len("X-Volume-GUIDs") + 1 :].split(","):
                query.volume_guids.append(guid.strip().lower())

    # run query
    with query:
        # don't allow scanning *all* files as it takes ~14h of CPU time
        if not query.user.check_acl("@admin"):
            if not query.component_content and not query.volume_guids:
                query.error = "no X-Volume-GUIDs specified"
                return

        component_ids = [
            x[0]
            for x in db.session.query(Component.component_id)
            .join(Firmware)
            .join(Remote)
            .filter(Remote.is_public)
            .all()
        ]
        if component_ids:
            fraction = 100 / len(component_ids)
            for idx, component_id in enumerate(component_ids):
                md = (
                    db.session.query(Component)
                    .filter(Component.component_id == component_id)
                    .one()
                )
                task.add_pass("Running Query", f"On {component_id} ({md.appstream_id})")

                # scan each shard
                for shard in md.shards:
                    if shard.guid in query.volume_guids:
                        _query_run_shard_yara(query, md, shard)
                        query.total += 1

                # scan each component
                if query.component_content:
                    _query_run_component_yara(query, md)
                query.found = len(query.results)
                query.percentage = (idx + 1) * fraction
                db.session.commit()


def _query_run_all(task: Task) -> None:
    # get all pending queries
    pending = (
        db.session.query(YaraQuery)
        .filter(YaraQuery.started_ts == None)
        .filter(YaraQuery.error == None)
    )
    for query in pending:
        _query_run(query, task=task)
