#!/usr/bin/python3
#
# Copyright 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=unused-argument,protected-access

import os
import hashlib
import hmac
import datetime
import json
from typing import List, Dict, Optional, Any

from flask import current_app as app

from lvfs import db
from lvfs.analytics.models import AnalyticVendorReports
from lvfs.vendors.models import Vendor, VendorAffiliation
from lvfs.components.models import Component
from lvfs.firmware.models import Firmware
from lvfs.metadata.models import Remote
from lvfs.tasks.models import Task
from lvfs.util import _get_datestr_from_datetime


def _vendor_hash(vendor: Vendor) -> str:
    """Generate a HMAC of the vendor name"""
    return hmac.new(
        key=app.config["SECRET_VENDOR_SALT"].encode(),
        msg=vendor.group_id.encode(),
        digestmod=hashlib.sha256,
    ).hexdigest()


def _update_trustedreports_vendor(task: Task, v: Vendor) -> None:

    # sanity check
    if not v.trusted_reports:
        return

    # find public firmware with at least one success report
    devices: List[Dict[str, Any]] = []
    for fw in (
        db.session.query(Firmware)
        .filter(Firmware.report_success_cnt > 0)
        .join(Remote)
        .filter(Remote.is_public)
    ):
        # check each OR clause
        for rule in v.trusted_reports.split(";"):
            if not fw._trusted_reports_for_rule(rule):
                continue
            md: Optional[Component] = fw.md_prio
            if not md:
                continue
            devices.append(
                {
                    "id": fw.firmware_id,
                    "remote": fw.remote.name,
                    "appstream_id": md.appstream_id,
                    "vendor": md.fw.vendor.display_name,
                    "name": md.name,
                    "version": md.version_display,
                }
            )
            break
    task.add_pass("Wrote File", f"trustedreports-{v.vendor_id}.json")
    with open(
        os.path.join(app.config["DOWNLOAD_DIR"], f"trustedreports-{v.vendor_id}.json"),
        "wb",
    ) as f:
        f.write(json.dumps(devices).encode())

    # update AnalyticVendorReports
    datestr: int = _get_datestr_from_datetime(datetime.datetime.today())
    db.session.query(AnalyticVendorReports).filter(
        AnalyticVendorReports.datestr == datestr
    ).delete()
    db.session.add(
        AnalyticVendorReports(vendor_id=v.vendor_id, datestr=datestr, cnt=len(devices))
    )


def _update_attrs_vendor(v: Vendor) -> None:
    # is an ODM if affiliation is set up
    v.is_odm = (
        db.session.query(VendorAffiliation.affiliation_id)
        .filter(VendorAffiliation.vendor_id_odm == v.vendor_id)
        .first()
        is not None
    )

    # 26 weeks is half a year or about 6 months
    now = datetime.datetime.utcnow() - datetime.timedelta(weeks=26)
    v.fws_stable_recent = (
        db.session.query(Firmware.firmware_id)
        .join(Firmware.remote)
        .filter(
            Remote.name == "stable",
            Firmware.vendor_id == v.vendor_id,
            Firmware.timestamp > now,
        )
        .count()
    )
    v.fws_stable = (
        db.session.query(Firmware.firmware_id)
        .join(Firmware.remote)
        .filter(Firmware.vendor_id == v.vendor_id, Remote.name == "stable")
        .count()
    )


def task_update_attrs_vendor(task: Task) -> None:
    v = db.session.query(Vendor).filter(Vendor.vendor_id == task.id).first()
    if v:
        _update_attrs_vendor(v)
        _update_trustedreports_vendor(task, v)
        db.session.commit()


def task_update_attrs(task: Task) -> None:
    for v in db.session.query(Vendor).with_for_update(of=Vendor):
        _update_attrs_vendor(v)
        _update_trustedreports_vendor(task, v)
    db.session.commit()
