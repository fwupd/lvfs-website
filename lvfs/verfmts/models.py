#!/usr/bin/python3
#
# Copyright 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods

import datetime

from typing import Optional

from sqlalchemy import Column, Integer, Text, DateTime

from lvfs import db


def _decode_bcd(val: int) -> int:

    return ((val >> 4) & 0x0F) * 10 + (val & 0x0F)


class Verfmt(db.Model):  # type: ignore
    __tablename__ = "verfmts"

    verfmt_id = Column(Integer, primary_key=True)
    value = Column(Text, nullable=False, unique=True)  # 'dell-bios'
    name = Column(Text, default=None)  # 'Dell Style'
    example = Column(Text, default=None)  # '12.34.56.78'
    fwupd_version = Column(Text, default=None)  # '1.3.3'
    fallbacks = Column(Text, default=None)  # 'quad,intelme'
    ctime = Column(DateTime, default=datetime.datetime.utcnow)

    @property
    def sections(self) -> int:
        if not self.example:
            return 0
        if self.value in ["plain", "bcd"]:
            return 0
        return len(self.example.split("."))

    def _uint32_to_str(self, v: int) -> Optional[str]:
        if self.value == "plain":
            return str(v)
        if self.value == "quad":
            return (
                f"{(v & 0xFF000000) >> 24}."
                f"{(v & 0x00FF0000) >> 16}."
                f"{(v & 0x0000FF00) >> 8}."
                f"{v & 0x000000FF}"
            )
        if self.value == "triplet":
            return (
                f"{(v & 0xFF000000) >> 24}."
                f"{(v & 0x00FF0000) >> 16}."
                f"{v & 0x0000FFFF}"
            )
        if self.value == "pair":
            return f"{(v & 0xFFFF0000) >> 16}.{v & 0x0000FFFF}"
        if self.value == "intel-me":
            return (
                f"{((v & 0xE0000000) >> 29) + 0x0B}."
                f"{(v & 0x1F000000) >> 24}."
                f"{(v & 0x00FF0000) >> 16}."
                f"{v & 0x0000FFFF}"
            )
        if self.value == "intel-csme19":
            return (
                f"{((v & 0xE0000000) >> 29) + 19}."
                f"{(v & 0x1F000000) >> 24}."
                f"{(v & 0x00FF0000) >> 16}."
                f"{v & 0x0000FFFF}"
            )
        if self.value == "intel-me2":
            return (
                f"{(v & 0xF0000000) >> 28}."
                f"{(v & 0x0F000000) >> 24}."
                f"{(v & 0x00FF0000) >> 16}."
                f"{v & 0x0000FFFF}"
            )
        if self.value == "surface-legacy":
            return f"{(v >> 22) & 0x3FF}.{(v >> 10) & 0xFFF}.{v & 0x3FF}"
        if self.value == "surface":
            return f"{(v >> 24) & 0xFF}.{(v >> 8) & 0xFFFF}.{v & 0xFF}"
        if self.value == "bcd":
            if v > 0xFFFF:
                return (
                    f"{_decode_bcd(v >> 24)}."
                    f"{_decode_bcd(v >> 16)}."
                    f"{_decode_bcd(v >> 8)}."
                    f"{_decode_bcd(v)}"
                )
            return f"{_decode_bcd(v >> 8)}.{_decode_bcd(v)}"
        if self.value == "dell-bios":
            return (
                f"{(v & 0x00FF0000) >> 16}."
                f"{(v & 0x0000FF00) >> 8}."
                f"{v & 0x000000FF}"
            )
        if self.value == "dell-bios-msb":
            return (
                f"{(v & 0xFF000000) >> 24}."
                f"{(v & 0x00FF0000) >> 16}."
                f"{(v & 0x0000FF00) >> 8}"
            )
        if self.value == "number":
            return str(v)
        if self.value == "hex":
            return f"{v:#08x}"
        return None

    def version_display(self, version: str) -> Optional[str]:
        if version.isdigit():
            return self._uint32_to_str(int(version))
        return version

    def __repr__(self) -> str:
        return f"Verfmt({self.verfmt_id},{self.value})"
