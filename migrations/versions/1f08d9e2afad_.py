"""empty message

Revision ID: 1f08d9e2afad
Revises: 65be7daff681
Create Date: 2024-09-10 10:37:52.650240

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "1f08d9e2afad"
down_revision = "65be7daff681"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("component_refs", schema=None) as batch_op:
        batch_op.drop_index("ix_component_refs_component_id")
        batch_op.drop_index("ix_component_refs_vendor_id")
        batch_op.drop_index("ix_component_refs_vendor_id_partner")
    op.drop_table("component_refs")


def downgrade():
    op.create_table(
        "component_refs",
        sa.Column("component_ref_id", sa.INTEGER(), nullable=False),
        sa.Column("component_id", sa.INTEGER(), nullable=True),
        sa.Column("vendor_id", sa.INTEGER(), nullable=False),
        sa.Column("vendor_id_partner", sa.INTEGER(), nullable=False),
        sa.Column("protocol_id", sa.INTEGER(), nullable=True),
        sa.Column("appstream_id", sa.TEXT(), nullable=True),
        sa.Column("version", sa.TEXT(), nullable=False),
        sa.Column("release_tag", sa.TEXT(), nullable=True),
        sa.Column("date", sa.DATETIME(), nullable=True),
        sa.Column("name", sa.TEXT(), nullable=False),
        sa.Column("url", sa.TEXT(), nullable=True),
        sa.Column("status", sa.TEXT(), nullable=True),
        sa.ForeignKeyConstraint(
            ["component_id"],
            ["components.component_id"],
        ),
        sa.ForeignKeyConstraint(
            ["protocol_id"],
            ["protocol.protocol_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id"],
            ["vendors.vendor_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id_partner"],
            ["vendors.vendor_id"],
        ),
        sa.PrimaryKeyConstraint("component_ref_id"),
    )
    with op.batch_alter_table("component_refs", schema=None) as batch_op:
        batch_op.create_index(
            "ix_component_refs_vendor_id_partner", ["vendor_id_partner"], unique=False
        )
        batch_op.create_index(
            "ix_component_refs_vendor_id", ["vendor_id"], unique=False
        )
        batch_op.create_index(
            "ix_component_refs_component_id", ["component_id"], unique=False
        )
