# Custom template
"""empty message

Revision ID: d38beff71281
Revises: dd7f73106512
Create Date: 2024-08-16 17:19:27.401768

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "d38beff71281"
down_revision = "dd7f73106512"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("test_attributes", schema=None) as batch_op:
        batch_op.add_column(sa.Column("url", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("test_attributes", schema=None) as batch_op:
        batch_op.drop_column("url")
