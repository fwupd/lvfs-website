# Custom template
"""empty message

Revision ID: 386e030ea731
Revises: be05e97f27ac
Create Date: 2025-01-07 11:30:53.403541

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "386e030ea731"
down_revision = "be05e97f27ac"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "protocol_verfmt_items",
        sa.Column("protocol_verfmt_item_id", sa.Integer(), nullable=False),
        sa.Column("protocol_id", sa.Integer(), nullable=True),
        sa.Column("verfmt_id", sa.Integer(), nullable=True),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["protocol_id"],
            ["protocol.protocol_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.ForeignKeyConstraint(
            ["verfmt_id"],
            ["verfmts.verfmt_id"],
        ),
        sa.PrimaryKeyConstraint("protocol_verfmt_item_id"),
    )


def downgrade():
    op.drop_table("protocol_verfmt_items")
