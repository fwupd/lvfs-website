"""empty message

Revision ID: 07ffe01a08e0
Revises: df24df93b244
Create Date: 2024-07-22 14:53:31.947048

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "07ffe01a08e0"
down_revision = "df24df93b244"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("report_attributes", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_report_attributes_key"), ["key"], unique=False
        )


def downgrade():
    with op.batch_alter_table("report_attributes", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_report_attributes_key"))
