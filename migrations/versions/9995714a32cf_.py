# Custom template
"""empty message

Revision ID: 9995714a32cf
Revises: 8eb9b0797e41
Create Date: 2024-03-11 12:45:05.527759

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "9995714a32cf"
down_revision = "8eb9b0797e41"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("issues", schema=None) as batch_op:
        batch_op.add_column(sa.Column("mtime", sa.DateTime(), nullable=True))


def downgrade():
    with op.batch_alter_table("issues", schema=None) as batch_op:
        batch_op.drop_column("mtime")
