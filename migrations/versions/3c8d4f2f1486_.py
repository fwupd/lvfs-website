from alembic import op
import sqlalchemy as sa
from sqlalchemy.exc import ProgrammingError


# revision identifiers, used by Alembic.
revision = "3c8d4f2f1486"
down_revision = "8f56e490c585"
branch_labels = None
depends_on = None


def upgrade():
    try:
        with op.batch_alter_table("firmware_revisions", schema=None) as batch_op:
            batch_op.add_column(sa.Column("mirror_url", sa.Text(), nullable=True))
    except ProgrammingError as e:
        print(str(e))


def downgrade():
    with op.batch_alter_table("firmware_revisions", schema=None) as batch_op:
        batch_op.drop_column("mirror_url")
