"""empty message

Revision ID: b9b0f5d08576
Revises: b8055424108c
Create Date: 2024-05-08 16:15:52.661762

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "b9b0f5d08576"
down_revision = "b8055424108c"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("hsi_reports", schema=None) as batch_op:
        batch_op.drop_column("signature")
        batch_op.drop_column("payload")


def downgrade():
    with op.batch_alter_table("hsi_reports", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("payload", sa.TEXT(), autoincrement=False, nullable=False)
        )
        batch_op.add_column(
            sa.Column("signature", sa.TEXT(), autoincrement=False, nullable=True)
        )
