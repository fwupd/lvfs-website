# Custom template
"""empty message

Revision ID: 867f881becfb
Revises: f0ec0c4e83bc
Create Date: 2024-05-09 18:11:30.885715

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "867f881becfb"
down_revision = "f0ec0c4e83bc"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("device_report_infos", schema=None) as batch_op:
        batch_op.add_column(sa.Column("icon", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("device_report_infos", schema=None) as batch_op:
        batch_op.drop_column("icon")
