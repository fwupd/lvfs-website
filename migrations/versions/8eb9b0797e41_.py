"""empty message

Revision ID: 8eb9b0797e41
Revises: 4c9d645e1d27
Create Date: 2024-02-23 16:47:04.816865

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8eb9b0797e41"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.add_column(sa.Column("trusted_reports", sa.Text(), nullable=True))
        batch_op.add_column(sa.Column("trusted_reports_url", sa.Text(), nullable=True))
        batch_op.add_column(sa.Column("distro_name", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.drop_column("trusted_reports")
        batch_op.drop_column("trusted_reports_url")
        batch_op.drop_column("distro_name")
