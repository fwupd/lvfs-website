"""empty message

Revision ID: 8f56e490c585
Revises: 87e6d334d4a0
Create Date: 2024-05-28 20:01:35.705367

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8f56e490c585"
down_revision = "87e6d334d4a0"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("firmware", schema=None) as batch_op:
        batch_op.add_column(sa.Column("mirror_url", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("firmware", schema=None) as batch_op:
        batch_op.drop_column("mirror_url")
