"""empty message

Revision ID: be05e97f27ac
Revises: 33bf4c994de8
Create Date: 2024-09-19 21:42:37.833877

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "be05e97f27ac"
down_revision = "33bf4c994de8"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "analytics_vendor_reports",
        sa.Column("analytic_vendor_report_id", sa.Integer(), nullable=False),
        sa.Column("datestr", sa.Integer(), nullable=True),
        sa.Column("vendor_id", sa.Integer(), nullable=False),
        sa.Column("cnt", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["vendor_id"],
            ["vendors.vendor_id"],
        ),
        sa.PrimaryKeyConstraint("analytic_vendor_report_id"),
    )
    with op.batch_alter_table("analytics_vendor_reports", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_analytics_vendor_reports_datestr"), ["datestr"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_analytics_vendor_reports_vendor_id"),
            ["vendor_id"],
            unique=False,
        )


def downgrade():
    with op.batch_alter_table("analytics_vendor_reports", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_analytics_vendor_reports_vendor_id"))
        batch_op.drop_index(batch_op.f("ix_analytics_vendor_reports_datestr"))
    op.drop_table("analytics_vendor_reports")
