"""empty message

Revision ID: 33bf4c994de8
Revises: 1f08d9e2afad
Create Date: 2024-09-17 14:53:53.533004

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "33bf4c994de8"
down_revision = "1f08d9e2afad"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.add_column(sa.Column("relationship_desc", sa.Text(), nullable=True))
        batch_op.add_column(sa.Column("relationship_url", sa.Text(), nullable=True))
        batch_op.add_column(sa.Column("relationship_tos", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.drop_column("relationship_tos")
        batch_op.drop_column("relationship_url")
        batch_op.drop_column("relationship_desc")
