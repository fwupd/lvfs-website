"""empty message

Revision ID: 65be7daff681
Revises: d38beff71281
Create Date: 2024-09-04 09:23:58.814626

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "65be7daff681"
down_revision = "d38beff71281"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("firmware_assets", schema=None) as batch_op:
        batch_op.add_column(sa.Column("mirror_url", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("firmware_assets", schema=None) as batch_op:
        batch_op.drop_column("mirror_url")
