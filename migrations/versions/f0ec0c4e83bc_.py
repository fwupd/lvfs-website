"""empty message

Revision ID: f0ec0c4e83bc
Revises: b9b0f5d08576
Create Date: 2024-05-09 15:09:09.012312

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "f0ec0c4e83bc"
down_revision = "b9b0f5d08576"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "device_report_infos",
        sa.Column("device_report_info_id", sa.Integer(), nullable=False),
        sa.Column("vendor_id", sa.Text(), nullable=False),
        sa.Column("instance_id", sa.Text(), nullable=False),
        sa.Column("cnt_supported", sa.Integer(), nullable=True),
        sa.Column("cnt_unsupported", sa.Integer(), nullable=True),
        sa.PrimaryKeyConstraint("device_report_info_id"),
    )
    with op.batch_alter_table("device_report_infos", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_device_report_infos_instance_id"),
            ["instance_id"],
            unique=True,
        )
        batch_op.create_index(
            batch_op.f("ix_device_report_infos_vendor_id"), ["vendor_id"], unique=False
        )

    op.create_table(
        "device_report",
        sa.Column("device_report_id", sa.Integer(), nullable=False),
        sa.Column("machine_id", sa.Text(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("device_report_id"),
    )
    with op.batch_alter_table("device_report", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_device_report_machine_id"), ["machine_id"], unique=False
        )

    op.create_table(
        "device_report_items",
        sa.Column("device_report_item_id", sa.Integer(), nullable=False),
        sa.Column("device_report_id", sa.Integer(), nullable=False),
        sa.Column("instance_id", sa.Text(), nullable=False),
        sa.Column("vendor_id", sa.Text(), nullable=False),
        sa.Column("plugin", sa.Text(), nullable=False),
        sa.Column("vendor", sa.Text(), nullable=False),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("supported", sa.Boolean(), nullable=True),
        sa.ForeignKeyConstraint(
            ["device_report_id"],
            ["device_report.device_report_id"],
        ),
        sa.PrimaryKeyConstraint("device_report_item_id"),
    )
    with op.batch_alter_table("device_report_items", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_device_report_items_device_report_id"),
            ["device_report_id"],
            unique=False,
        )
        batch_op.create_index(
            batch_op.f("ix_device_report_items_instance_id"),
            ["instance_id"],
            unique=False,
        )
        batch_op.create_index(
            batch_op.f("ix_device_report_items_name"), ["name"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_device_report_items_plugin"), ["plugin"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_device_report_items_vendor"), ["vendor"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_device_report_items_vendor_id"), ["vendor_id"], unique=False
        )


def downgrade():
    with op.batch_alter_table("device_report_items", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_device_report_items_vendor_id"))
        batch_op.drop_index(batch_op.f("ix_device_report_items_vendor"))
        batch_op.drop_index(batch_op.f("ix_device_report_items_plugin"))
        batch_op.drop_index(batch_op.f("ix_device_report_items_name"))
        batch_op.drop_index(batch_op.f("ix_device_report_items_instance_id"))
        batch_op.drop_index(batch_op.f("ix_device_report_items_device_report_id"))

    op.drop_table("device_report_items")
    with op.batch_alter_table("device_report", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_device_report_machine_id"))

    op.drop_table("device_report")
    with op.batch_alter_table("device_report_infos", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_device_report_infos_vendor_id"))
        batch_op.drop_index(batch_op.f("ix_device_report_infos_instance_id"))

    op.drop_table("device_report_infos")
