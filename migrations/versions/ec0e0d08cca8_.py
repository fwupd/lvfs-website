"""empty message

Revision ID: ec0e0d08cca8
Revises: 3c8d4f2f1486
Create Date: 2024-06-13 10:17:07.582653

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "ec0e0d08cca8"
down_revision = "3c8d4f2f1486"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("requirements", schema=None) as batch_op:
        batch_op.add_column(sa.Column("ctime", sa.DateTime(), nullable=True))
        batch_op.add_column(sa.Column("user_id", sa.Integer(), nullable=True))
        batch_op.create_foreign_key(None, "users", ["user_id"], ["user_id"])


def downgrade():
    with op.batch_alter_table("requirements", schema=None) as batch_op:
        batch_op.drop_constraint(None, type_="foreignkey")
        batch_op.drop_column("user_id")
        batch_op.drop_column("ctime")
