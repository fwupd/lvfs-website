"""empty message

Revision ID: 8737a846ab49
Revises: ec0e0d08cca8
Create Date: 2024-07-11 13:07:16.035822

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8737a846ab49"
down_revision = "ec0e0d08cca8"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("reports", schema=None) as batch_op:
        batch_op.add_column(sa.Column("addr", sa.Text(), nullable=True))
        batch_op.create_index(batch_op.f("ix_reports_addr"), ["addr"], unique=False)


def downgrade():
    with op.batch_alter_table("reports", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_reports_addr"))
        batch_op.drop_column("addr")
