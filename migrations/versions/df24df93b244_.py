"""empty message

Revision ID: df24df93b244
Revises: 8737a846ab49
Create Date: 2024-07-22 13:56:13.431625

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "df24df93b244"
down_revision = "8737a846ab49"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "report_attribute_infos",
        sa.Column("report_attribute_id", sa.Integer(), nullable=False),
        sa.Column("key", sa.Text(), nullable=False),
        sa.Column("cnt", sa.Integer(), nullable=True),
        sa.PrimaryKeyConstraint("report_attribute_id"),
    )
    with op.batch_alter_table("report_attribute_infos", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_report_attribute_infos_key"), ["key"], unique=False
        )


def downgrade():
    with op.batch_alter_table("report_attribute_infos", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_report_attribute_infos_key"))
    op.drop_table("report_attribute_infos")
