"""empty message

Revision ID: 52850db6d284
Revises: af63bf5e629c
Create Date: 2024-04-29 09:44:06.381703

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "52850db6d284"
down_revision = "af63bf5e629c"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "license_alias",
        sa.Column("license_alias_id", sa.Integer(), nullable=False),
        sa.Column("license_id", sa.Integer(), nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["license_id"],
            ["licenses.license_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("license_alias_id"),
        sa.UniqueConstraint(
            "license_id", "value", name="uq_component_alias_license_id_value"
        ),
        sa.UniqueConstraint("value"),
    )
    with op.batch_alter_table("license_alias", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_license_alias_license_id"), ["license_id"], unique=False
        )


def downgrade():
    with op.batch_alter_table("license_alias", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_license_alias_license_id"))
    op.drop_table("license_alias")
