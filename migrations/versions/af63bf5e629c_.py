"""empty message

Revision ID: af63bf5e629c
Revises: 909f8e2783f4
Create Date: 2024-04-22 15:05:20.003619

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "af63bf5e629c"
down_revision = "909f8e2783f4"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("component_licenses", schema=None) as batch_op:
        batch_op.alter_column("license_id", existing_type=sa.INTEGER(), nullable=False)


def downgrade():
    with op.batch_alter_table("component_licenses", schema=None) as batch_op:
        batch_op.alter_column("license_id", existing_type=sa.INTEGER(), nullable=True)
