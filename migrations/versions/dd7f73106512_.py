# Custom template
"""empty message

Revision ID: dd7f73106512
Revises: 07ffe01a08e0
Create Date: 2024-08-15 13:46:41.744281

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "dd7f73106512"
down_revision = "07ffe01a08e0"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("tasks", schema=None) as batch_op:
        batch_op.add_column(sa.Column("success", sa.Boolean(), nullable=True))


def downgrade():
    with op.batch_alter_table("tasks", schema=None) as batch_op:
        batch_op.drop_column("success")
