# Custom template
"""empty message

Revision ID: 87e6d334d4a0
Revises: 867f881becfb
Create Date: 2024-05-28 10:53:44.202222

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "87e6d334d4a0"
down_revision = "867f881becfb"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("users", schema=None) as batch_op:
        batch_op.alter_column(
            "password",
            existing_type=sa.VARCHAR(length=128),
            type_=sa.Text(),
            existing_nullable=True,
        )


def downgrade():
    with op.batch_alter_table("users", schema=None) as batch_op:
        batch_op.alter_column(
            "password",
            existing_type=sa.Text(),
            type_=sa.VARCHAR(length=128),
            existing_nullable=True,
        )
