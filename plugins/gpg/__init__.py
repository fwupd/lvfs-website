#!/usr/bin/python3
#
# Copyright 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import time
from typing import Optional
import gnupg

from jcat import JcatBlobText, JcatBlobKind
from lvfs.pluginloader import PluginBase, PluginError, PluginSetting, PluginSettingBool


class Affidavit:

    """A quick'n'dirty signing server"""

    def __init__(self, key_uid: Optional[str] = None, homedir: str = "/tmp") -> None:
        """Set defaults"""

        # check exists
        if not os.path.exists(homedir):
            try:
                os.mkdir(homedir)
            except OSError as e:
                raise PluginError from e

        # GPG will sometimes refuse to return a version --
        # workaround as the entire gnupg architecture is upside-down and unfixable BUT the
        # much better pysequoia needs a Rust toolchain to be installed...
        self._keyid = None
        for _ in range(50):
            gpg = gnupg.GPG(gnupghome=homedir, gpgbinary="gpg2")
            gpg.encoding = "utf-8"
            if gpg.version:
                break
            print("no GPG version, trying again")
            time.sleep(0.05)
        if not gpg.version:
            raise PluginError("No GPG version detected")

        # find correct key ID for the UID
        for privkey in gpg.list_keys(secret=True):
            for uid in privkey["uids"]:
                if uid.find(key_uid) != -1:
                    self._keyid = privkey["keyid"]
        if not self._keyid:
            raise PluginError(f"No imported private key for {key_uid}")
        self._homedir = homedir

    def create(self, data: bytes) -> bytes:
        """Create detached signature data"""
        gpg = gnupg.GPG(gnupghome=self._homedir, gpgbinary="gpg2")
        gpg.encoding = "utf-8"
        return gpg.sign(data, detach=True, keyid=self._keyid)

    def create_detached(self, filename: str) -> str:
        """Create a detached signature file"""
        with open(filename, "rb") as fin:
            data = fin.read()
            with open(filename + ".asc", "wb") as f:
                f.write(self.create(data))
        return filename + ".asc"

    def verify(self, data: bytes) -> bool:
        """Verify that the data was signed by something we trust"""
        gpg = gnupg.GPG(gnupghome=self._homedir, gpgbinary="gpg2")
        gpg.encoding = "utf-8"
        ver = gpg.verify(data)
        if not ver.valid:
            raise PluginError("Firmware was signed with an unknown private key")
        return True


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "sign-gpg")
        self.name = "GPG Signing"
        self.summary = (
            "Sign files using GnuPG, a free implementation of the OpenPGP standard"
        )
        self.settings.append(PluginSettingBool(key="sign_gpg_enable", name="Enabled"))
        self.settings.append(
            PluginSetting(
                key="sign_gpg_keyring_dir",
                name="Keyring Directory",
                default="/var/www/lvfs/.gnupg",
            )
        )
        self.settings.append(
            PluginSetting(
                key="sign_gpg_firmware_uid",
                name="Signing UID for firmware",
                default="sign-test@fwupd.org",
            )
        )
        self.settings.append(
            PluginSetting(
                key="sign_gpg_metadata_uid",
                name="Signing UID for metadata",
                default="sign-test@fwupd.org",
            )
        )

    def metadata_sign(self, blob):
        # create the detached signature
        affidavit = Affidavit(
            self.get_setting("sign_gpg_firmware_uid", required=True),
            self.get_setting("sign_gpg_keyring_dir", required=True),
        )
        contents_asc = str(affidavit.create(blob))
        return JcatBlobText(JcatBlobKind.GPG, contents_asc)

    def archive_sign(self, blob):
        # create the detached signature
        affidavit = Affidavit(
            self.get_setting("sign_gpg_firmware_uid", required=True),
            self.get_setting("sign_gpg_keyring_dir", required=True),
        )
        contents_asc = str(affidavit.create(blob))
        return JcatBlobText(JcatBlobKind.GPG, contents_asc)
