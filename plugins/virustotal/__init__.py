#!/usr/bin/python3
#
# Copyright 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Optional
import requests

from lvfs.pluginloader import PluginBase, PluginError
from lvfs.pluginloader import PluginSetting, PluginSettingBool, PluginSettingList
from lvfs.tests.models import Test
from lvfs.firmware.models import Firmware
from lvfs.components.models import Component


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "virustotal")
        self.name = "VirusTotal Monitor"
        self.summary = "Upload firmware to VirusTotal for false-positive detection"
        self.settings.append(
            PluginSettingBool(key="virustotal_enable", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingList(
                key="virustotal_remotes",
                name="Upload Firmware in Remotes",
                default=["stable", "testing"],
            )
        )
        self.settings.append(
            PluginSetting(key="virustotal_api_key", name="API Key", default="DEADBEEF")
        )
        self.settings.append(
            PluginSetting(
                key="virustotal_uri",
                name="Host",
                default="https://www.virustotal.com/api/v3/monitor/items",
            )
        )
        self.settings.append(
            PluginSetting(
                key="virustotal_user_agent", name="User Agent", default="LVFS"
            )
        )

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # is the firmware not in a correct remote
        remotes = self.get_setting_list("virustotal_remotes", required=True)
        if fw.remote.name not in remotes:
            return

        # add if not already exists on any component in the firmware
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:
        # sanity check
        blob: Optional[bytes] = md.read()
        if not blob:
            return

        # 32MB limit
        if len(blob) > 0x2000000:
            return

        # build the remote name
        remote_path = (
            "/"
            + md.fw.vendor.group_id
            + "/"
            + str(md.component_id)
            + "/"
            + md.filename_contents
        )

        # upload the file
        try:
            headers: dict[str, str] = {}
            headers["X-Apikey"] = self.get_setting("virustotal_api_key", required=True)
            headers["User-Agent"] = self.get_setting(
                "virustotal_user_agent", required=True
            )
            url = self.get_setting("virustotal_uri", required=True)
            files = {"file": ("filepath", blob, "application/octet-stream")}
            args = {"path": remote_path}
            try:
                r = requests.post(
                    url,
                    files=files,
                    data=args,
                    headers=headers,
                    timeout=60,
                )
            except (
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
            ) as e:
                raise PluginError(f"Timeout from {url}") from e
            if r.status_code != 200:
                test.add_warn("Failed Upload", r.text)
                return
        except IOError as e:
            raise PluginError from e


# run with PYTHONPATH=. ./env/bin/python3 plugins/virustotal/__init__.py
if __name__ == "__main__":
    import sys

    from lvfs import create_app
    from lvfs.vendors.models import Vendor

    with create_app().app_context():
        plugin = Plugin()
        _test = Test(plugin_id=plugin.id)
        _vendor = Vendor()
        _vendor.group_id = "test"
        _fw = Firmware()
        _fw.vendor = _vendor
        _md = Component()
        _md.component_id = 1
        _md.filename_contents = "firmware.bin"
        _fw.mds.append(_md)
        for _argv in sys.argv[1:]:
            with open(_argv, "rb") as f:
                _md.write(f.read())
            plugin.run_test_on_md(_test, _md)
            for attribute in _test.attributes:
                print(attribute)
