#!/usr/bin/python3
#
# Copyright 2025 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=protected-access

import json
import datetime

import requests

from lvfs.firmware.models import Firmware
from lvfs.components.models import Component, ComponentIssue
from lvfs.pluginloader import PluginBase, PluginError, PluginSettingBool


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "cveproject")
        self.order_after = ["vince"]
        self.name = "CVEProject"
        self.summary = "CVE List V5, the official CVE List"
        self.settings.append(PluginSettingBool(key="cveproject_enable", name="Enabled"))

    def _update_issue_json(self, issue: ComponentIssue, r_text: str) -> None:
        try:
            data = json.loads(r_text)
        except json.decoder.JSONDecodeError as e:
            raise PluginError(f"Failed to query: {r_text}") from e
        try:
            issue.description = data["containers"]["cna"]["descriptions"][0]["value"]
        except (KeyError, IndexError, ValueError):
            pass
        try:
            issue.published = datetime.datetime.fromisoformat(
                data["cveMetadata"]["datePublished"]
            )
        except (KeyError, IndexError, ValueError):
            pass

    def _update_issue(self, issue: ComponentIssue) -> None:
        try:
            _, issue_year, issue_number = issue.value.split("-")
        except ValueError:
            return
        api = (
            "https://raw.githubusercontent.com/CVEProject/cvelistV5/refs/heads/main/cves/"
            + f"{issue_year}/{issue_number[:-3]}xxx/{issue.value}.json"
        )
        print(f"requesting {api}…")
        try:
            r = requests.get(api, stream=True, timeout=10)
        except (
            requests.exceptions.ConnectionError,
            requests.exceptions.ReadTimeout,
        ) as e:
            raise PluginError(f"Timeout from {api}") from e
        if r.status_code == 200:
            self._update_issue_json(issue, r.text)

    def archive_presign(self, fw: Firmware) -> None:
        # add description and published dates for CVEs
        for md in fw.mds:
            for issue in md.issues:
                if issue.kind == "cve":
                    if not issue.description or not issue.published:
                        self._update_issue(issue)


# run with PYTHONPATH=. ./env/bin/python3 plugins/cveproject/__init__.py
if __name__ == "__main__":

    from lvfs import create_app

    with create_app().app_context():
        plugin = Plugin()
        _fw = Firmware()
        _md = Component()
        _md.issues.append(ComponentIssue(kind="cve", value="CVE-2020-0545"))
        _md.issues.append(ComponentIssue(kind="cve", value="CVE-2020-8696"))
        _fw.mds.append(_md)
        plugin.archive_presign(_fw)
        for _issue in _md.issues:
            print(f"issue {_issue.value} = {_issue.description} [{_issue.published}]")
