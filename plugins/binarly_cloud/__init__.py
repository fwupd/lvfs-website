#!/usr/bin/python3
#
# Copyright 2024 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=protected-access

from typing import Optional
import json
import sys
import time

import requests
from flask import url_for
from sqlalchemy.exc import NoResultFound

from lvfs import db
from lvfs.pluginloader import PluginBase, PluginError
from lvfs.pluginloader import PluginSetting, PluginSettingBool, PluginSettingList
from lvfs.tests.models import Test, TestAttribute, TestResult
from lvfs.firmware.models import Firmware
from lvfs.components.models import Component
from lvfs.vendors.models import Vendor


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "binarly_cloud")
        self.name = "Binarly Cloud"
        self.summary = (
            "Binary Risk Hunt is a free service that helps security teams and "
            + "product owners validate their firmware and software supply chain is "
            + "truly composed of."
        )
        self.settings.append(
            PluginSettingBool(key="binarly_cloud_enable", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingList(
                key="binarly_cloud_remotes",
                name="Upload Firmware in Remotes",
                default=["stable", "testing", "embargo"],
            )
        )
        self.settings.append(
            PluginSetting(
                key="binarly_cloud_api_key", name="API Key", default="fwhunt_DEADBEEF"
            )
        )
        self.settings.append(
            PluginSetting(
                key="binarly_cloud_uri",
                name="Host",
                default="https://risk-api.binarly.io/scan",
            )
        )
        self.settings.append(
            PluginSetting(
                key="binarly_cloud_user_agent", name="User Agent", default="LVFS"
            )
        )

    def require_test_for_md(self, md: Component) -> bool:
        if not md.protocol:
            return False
        return md.protocol.value == "org.uefi.capsule"

    def ensure_test_for_fw(self, fw: Firmware) -> None:

        # is the firmware not in a correct remote
        remotes = self.get_setting_list("binarly_cloud_remotes", required=True)
        if fw.remote.key not in remotes:
            return

        # add if not already exists on any component in the firmware
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def _parse_results(self, test: Test, text: str) -> bool:

        data = json.loads(text)
        if "scan" not in data:
            return False
        for finding in data["scan"]["findings"]:
            try:
                title: Optional[str] = " & ".join(finding["identifiers"])
                msg: Optional[str] = finding.get("slug")
                url: Optional[str] = None
                for issue_id in finding["identifiers"]:
                    if issue_id.startswith("BRLY-"):
                        url = f"https://www.binarly.io/advisories/{issue_id.lower()}"
                        break
                if not url:
                    for issue_id in finding["identifiers"]:
                        if issue_id.startswith("CVE-"):
                            url = f"https://nvd.nist.gov/vuln/detail/{issue_id}"
                            break
                severity = finding.get("severity", "UNKNOWN")
                if severity == "CRITICAL":
                    test.add_warn(
                        title=f"Detected {title}",
                        message=f"Critical security issue detected: {msg}",
                        url=url,
                    )
                elif severity in ["UNKNOWN", "MEDIUM"]:
                    test.add_warn(
                        title=f"Detected {title}",
                        message=f"Security issue detected: {msg}",
                        url=url,
                    )
                else:
                    test.add_warn(
                        title=f"Detected {title}",
                        message=f"Issue ({severity} severity) detected: {msg}",
                        url=url,
                    )
            except AttributeError:
                pass

        # success
        return True

    def _get_results(self, test: Test, scan_id: str) -> bool:

        # get results
        api_key = self.get_setting("binarly_cloud_api_key", required=True)
        user_agent = self.get_setting("binarly_cloud_user_agent", required=True)
        url = self.get_setting("binarly_cloud_uri", required=True)
        headers: dict[str, str] = {
            "Authorization": f"Bearer {api_key}",
            "User-Agent": user_agent,
        }
        try:
            r = requests.get(
                f"{url}/{scan_id}", headers=headers, stream=True, timeout=60
            )
        except (
            requests.exceptions.ReadTimeout,
            requests.exceptions.ConnectionError,
        ) as e:
            raise PluginError(f"Timeout from {url}") from e
        if r.status_code != 200:
            if r.text.find("analysis timeout") != -1:
                test.add_warn("Failed to get results", r.text)
            else:
                test.add_fail("Failed to get results", r.text)
            return False
        if not r.text:
            test.add_fail("No GET response")
            return False
        return self._parse_results(test=test, text=r.text)

    def run_test_on_md(self, test: Test, md: Component) -> None:

        # already run, waiting for results
        attr = test.get_attr("ScanId")
        if attr:
            self._get_results(test, attr.message)
            return

        # sanity check
        blob: Optional[bytes] = md.read()
        if not blob:
            return

        # 100MB limit
        if len(blob) > 0x6400000:
            test.add_warn("Firmware binary too large")
            return

        # has the vendor opted-in to scanning
        try:
            vendor_binarly = (
                db.session.query(Vendor).filter(Vendor.group_id == "binarly").one()
            )
        except NoResultFound:
            test.add_warn(
                "Vendor Not Found",
                message="No binarly vendor group found",
            )
            return
        vendor = md.fw.vendor
        if not vendor.is_relationship(vendor_binarly.vendor_id):
            test.add_warn(
                f"{vendor.display_name_with_team} has no relationship with {vendor_binarly.display_name_with_team}",
                message=vendor_binarly.relationship_desc,
                url=url_for("vendors.route_relationships", vendor_id=vendor.vendor_id),
            )
            return

        # upload the file
        try:
            api_key = self.get_setting("binarly_cloud_api_key", required=True)
            user_agent = self.get_setting("binarly_cloud_user_agent", required=True)
            headers: dict[str, str] = {
                "Authorization": f"Bearer {api_key}",
                "User-Agent": user_agent,
                "X-LVFS-Vendor": md.fw.vendor.group_id,
                "X-LVFS-Component-Name": md.name.strip(),
            }
            url = self.get_setting("binarly_cloud_uri", required=True)
            files = {"file": ("filepath", blob, "application/octet-stream")}
            try:
                r = requests.post(
                    url,
                    files=files,
                    headers=headers,
                    timeout=300,
                )
            except (
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
                requests.exceptions.SSLError,
            ) as e:
                raise PluginError(f"Timeout from {url}") from e
            if r.status_code != 200:
                test.add_fail("Failed to upload", r.text)
                return
            if not r.text:
                test.add_fail("No POST response")
                return

            # parse response for the status and scan-id
            try:
                data = json.loads(r.text)
                if data["status"] != "ok":
                    test.add_fail("Error", data.get("error", "failed"))
                    return
                scan_id: str = data["id"]
            except ValueError as e:
                raise PluginError(f"Failed to parse response: {r.text}") from e

            # now poll for the result
            for i in range(120):
                time.sleep(5)
                if self._get_results(test, scan_id):
                    test.add_pass(
                        "Results", f"Got result for ScanId {scan_id} in {i*5} seconds."
                    )
                    break

        except IOError as e:
            raise PluginError from e


# run with PYTHONPATH=. ./env/bin/python3 plugins/binarly_cloud/__init__.py
if __name__ == "__main__":

    from lvfs import create_app
    from lvfs.protocols.models import Protocol

    with create_app().app_context():
        for _argv in sys.argv[1:]:
            print("Processing", _argv)

            plugin = Plugin()
            _test = Test()
            if _argv.endswith(".json"):
                with open(_argv, "rb") as f:
                    plugin._parse_results(_test, f.read().decode())
                for _attr in _test.attributes:
                    print(f"{_attr}")
                continue
            _fw = Firmware()
            _md = Component()
            _md.component_id = 999999
            _md.filename_contents = "firmware.bin"
            _md.protocol = Protocol(value="org.uefi.capsule")
            _fw.mds.append(_md)
            with open(_argv, "rb") as f:
                _fw.write(f.read())
            plugin.run_test_on_md(_test, _md)
            for _attr in _test.attributes:
                print(f"{_attr}")
