OAuth
=====

To test this with authentik, see https://docs.goauthentik.io/docs/installation/docker-compose:

    wget https://goauthentik.io/docker-compose.yml
    ...
    docker compose pull
    docker compose up -d

Then you can set up users from http://localhost:9000/if/flow/initial-setup/ -- the default plugin
values should then work once the `oauth_client_id` and `oauth_client_secret` have been set.

Azure
-----

To use Azure, use:

    oauth_scope: offline_access User.Read
    oauth_api_base_url: https://graph.microsoft.com/v1.0/
    oauth_access_token_url: https://login.microsoftonline.com/common/oauth2/v2.0/token
    oauth_authorize_url: https://login.microsoftonline.com/common/oauth2/v2.0/authorize
    oauth_userinfo: me

Other Providers
---------------

This plugin tries to work with other providers, and more workarounds may be copied from
https://flask-appbuilder.readthedocs.io/en/latest/_modules/flask_appbuilder/security/manager.html
