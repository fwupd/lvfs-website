#!/usr/bin/python3
#
# Copyright (C) 2024 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-member,unexpected-keyword-arg,raise-missing-from

import uuid
import jwt

from flask import session, request
from authlib.integrations.flask_client import OAuthError

from lvfs import oauth
from lvfs.pluginloader import PluginBase, PluginSetting, PluginSettingBool, PluginError
from lvfs.util import _get_settings


def _parse_me(me: dict[str, str]) -> dict[str, str]:

    data: dict[str, str] = {}

    # of course this is not specified
    for key in ["name", "nickname", "displayName"]:
        if key in me:
            data["name"] = me[key]
            break
    for key in ["email", "email-address", "upn"]:
        if key in me:
            data["email"] = me[key]
            break

    # fall back to first and last names
    if "displayName" not in data and ("given_name" in me and "family_name" in me):
        data["name"] = me["given_name"] + " " + me["family_name"]
    if "displayName" not in data and ("firstName" in me and "lastName" in me):
        data["name"] = me["firstName"] + " " + me["lastName"]

    # success
    return data


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "oauth")
        self.name = "OAuth"
        self.summary = "Authenticate using Generic OAuth provider"
        self.settings.append(PluginSettingBool(key="oauth_enable", name="Enabled"))
        self.settings.append(
            PluginSetting(key="oauth_name", name="Name", default="Authentik")
        )
        self.settings.append(PluginSetting(key="oauth_client_id", name="Client ID"))
        self.settings.append(
            PluginSetting(key="oauth_client_secret", name="Client Secret")
        )
        self.settings.append(
            PluginSetting(key="oauth_scope", name="Scope", default="profile+email")
        )
        self.settings.append(
            PluginSetting(
                key="oauth_api_base_url",
                name="API Base URL",
                default="http://localhost:9000/application/o",
            )
        )
        self.settings.append(
            PluginSetting(
                key="oauth_access_token_url",
                name="Access Token URL",
                default="http://localhost:9000/application/o/token/",
            )
        )
        self.settings.append(
            PluginSetting(
                key="oauth_authorize_url",
                name="Authorize URL",
                default="http://localhost:9000/application/o/authorize/",
            )
        )
        self.settings.append(
            PluginSetting(
                key="oauth_userinfo",
                name="User Info",
                default="userinfo",
            )
        )

        # register
        settings = _get_settings("oauth")
        if settings.get("oauth_client_id"):
            oauth.register(
                name="oauth",
                client_id=settings["oauth_client_id"],
                client_secret=settings["oauth_client_secret"],
                client_kwargs={"scope": settings["oauth_scope"]},
                api_base_url=settings["oauth_api_base_url"],
                access_token_url=settings["oauth_access_token_url"],
                authorize_url=settings["oauth_authorize_url"],
            )

    def oauth_authorize(self, callback):

        client = oauth.create_client("oauth")
        if not client:
            raise PluginError("No oauth client")
        return client.authorize_redirect(callback)

    def oauth_get_data(self):

        # get response success
        try:
            client = oauth.create_client("oauth")
            oauth_response = client.authorize_access_token()
        except (OAuthError, KeyError) as e:
            raise PluginError(f"OAuth failed: {e}")
        if oauth_response is None:
            raise PluginError("Access Denied" + str(request))

        # save the access token -- treat as a password
        try:
            session["oauth_token"] = (oauth_response["access_token"], "")
        except KeyError:
            raise PluginError("Access Denied, no oauth_token")

        # decode the JWT token
        try:
            me = jwt.decode(
                oauth_response["access_token"], options={"verify_signature": False}
            )
        except (KeyError, jwt.exceptions.DecodeError):
            raise PluginError("Access Denied, no access_token")
        data = _parse_me(me)

        # fall back to getting userinfo
        if "name" not in data and "email" not in data:
            resp = client.get(self.get_setting("oauth_userinfo", required=True))
            if not resp:
                raise PluginError("No suitable profile response")
            if not resp.data:
                raise PluginError("No suitable profile data")
            return _parse_me(resp.data)

        # success
        return data

    def oauth_logout(self):
        session.pop("oauth_token", None)
