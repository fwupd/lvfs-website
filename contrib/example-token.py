#!/usr/bin/python3
#
# Copyright 2024 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import sys
import base64
import json

from typing import Any, List

import requests


def _get_firmware(session: requests.Session, firmware_id: int) -> dict[str, Any]:

    try:
        basic_token = base64.b64encode(
            f"{os.environ['LVFS_USERNAME']}:{os.environ['LVFS_TOKEN']}".encode()
        ).decode()
        rv = session.get(
            os.path.join(
                os.environ["LVFS_SERVER"], "lvfs", "firmware", str(firmware_id), "json"
            ),
            headers={
                "User-Agent": "example-token.py",
                "Authorization": f"Basic {basic_token}",
            },
            timeout=30,
            verify=False,
        )
        if rv.status_code == 201:
            raise TypeError(f"API error {rv.status_code}")
        blob = json.loads(rv.content.decode())
        if blob.get("status") != "success":
            raise TypeError(blob.get("error"))
    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ReadTimeout,
        json.decoder.JSONDecodeError,
    ) as e:
        raise TypeError from e
    return blob


def _modify_component(
    session: requests.Session, component_id: int, data: dict[str, str]
) -> None:

    try:
        basic_token = base64.b64encode(
            f"{os.environ['LVFS_USERNAME']}:{os.environ['LVFS_TOKEN']}".encode()
        ).decode()
        rv = session.post(
            os.path.join(
                os.environ["LVFS_SERVER"],
                "lvfs",
                "components",
                str(component_id),
                "modify",
                "json",
            ),
            headers={
                "User-Agent": "example-token.py",
                "Authorization": f"Basic {basic_token}",
            },
            data=data,
            timeout=30,
            verify=False,
        )
        if rv.status_code == 201:
            raise TypeError(f"API error {rv.status_code}")
        blob = json.loads(rv.content.decode())
        if blob.get("status") != "success":
            raise TypeError(blob.get("error"))
    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ReadTimeout,
        json.decoder.JSONDecodeError,
    ) as e:
        raise TypeError from e

    return blob


def _fixup_dell_firmware(session: requests.Session, firmware_ids: List[int]) -> None:
    for firmware_id in firmware_ids:
        blob = _get_firmware(session, firmware_id)

        # check
        if blob["components"][0]["version_format"] != "dell-bios":
            print(f"skipping {firmware_id} as not dell-bios")
            continue
        component_id: int = int(blob["components"][0]["component_id"])
        version: int = int(blob["components"][0]["version"])
        _modify_component(
            session,
            component_id,
            {"version_format": "dell-bios-msb", "version": str(version << 8)},
        )


if __name__ == "__main__":
    # check required env variables are present
    for reqd_env in ["LVFS_USERNAME", "LVFS_TOKEN", "LVFS_SERVER"]:
        if reqd_env not in os.environ:
            print(f"Usage: {reqd_env} required")
            sys.exit(1)

    _session = requests.Session()
    _fixup_dell_firmware(_session, [1, 2, 3])
