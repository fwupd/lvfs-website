FROM quay.io/centos/centos:stream9
EXPOSE 5000

RUN dnf install -y epel-release dnf-plugins-core \
	&& dnf config-manager --set-enabled epel \
	&& dnf -y copr enable rhughes/lvfs-website \
	&& dnf -y --setopt=install_weak_deps=False --best install \
	git \
	bsdtar \
	jq \
	gcc \
	gnutls-utils \
	python3.12-devel \
	UEFITool \
	&& rm -rf /var/cache/dnf

# create all our dirs
RUN bash -c 'mkdir -p /app/{scripts,conf,logs/uwsgi}' \
	&& mkdir /data /backups

# save the git metadata
ARG CI_COMMIT_SHORT_SHA=000000000000
RUN echo ${CI_COMMIT_SHORT_SHA} > /app/GIT_HASH

WORKDIR /app

# create and activate a venv
ENV VIRTUAL_ENV=/app/venv
RUN python3.12 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# install all the things
COPY requirements.txt /app/conf
RUN pip3.12 install --no-cache-dir --upgrade pip \
	&& pip3.12 install --no-cache-dir wheel \
	&& pip3.12 install --no-cache-dir -r conf/requirements.txt

# and datadog
RUN pip3.12 install --no-cache-dir ddtrace==2.9.1

# copy the app; various configs and scripts
COPY wsgi.py /app/
COPY lvfs/ /app/lvfs/
COPY pkgversion/ /app/pkgversion/
COPY jcat/ /app/jcat/
COPY plugins/ /app/plugins/
COPY migrations/ /app/migrations/
COPY docker/files/application/gunicorn.py /app/conf/gunicorn.py
COPY docker/files/application/flaskapp.cfg /app/lvfs/flaskapp.cfg
COPY docker/files/lvfs-entrypoint.sh /app/lvfs-entrypoint.sh

RUN chown -R 65534:65534 /app /data /backups

ENTRYPOINT [ "./lvfs-entrypoint.sh" ]
